package acina.creative.ashwood.wallet.view_model;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

import acina.creative.ashwood.wallet.database.WalletDatabase;
import acina.creative.ashwood.wallet.database.expense.ExpenseEntry;
import timber.log.Timber;

public class DailyStatViewModel extends AndroidViewModel {

    private LiveData<List<ExpenseEntry>> expensesByCategories;
    private WalletDatabase mDb;

    public DailyStatViewModel(@NonNull Application application) {
        super(application);
        mDb = WalletDatabase.getInstance(this.getApplication());
        Timber.d("Actively retrieving the entries from the DataBase" );
        expensesByCategories = mDb.expenseDao().loadSumExpensesByCategoriesDaily();
    }

    public LiveData<List<ExpenseEntry>> getExpensesByCategories() {
        return expensesByCategories;
    }
}
