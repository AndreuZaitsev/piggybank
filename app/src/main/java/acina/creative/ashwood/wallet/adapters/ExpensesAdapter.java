package acina.creative.ashwood.wallet.adapters;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

import acina.creative.ashwood.wallet.R;
import acina.creative.ashwood.wallet.database.WalletDatabase;
import acina.creative.ashwood.wallet.database.category.CategoryEntry;
import acina.creative.ashwood.wallet.database.expense.ExpenseEntry;
import acina.creative.ashwood.wallet.database.income.IncomeEntry;
import acina.creative.ashwood.wallet.interfaces.ItemClickListener;
import acina.creative.ashwood.wallet.utils.AppExecutors;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ExpensesAdapter extends RecyclerView.Adapter<ExpensesAdapter.ExpensesViewHolder> {

    final private ItemClickListener mItemClickListener;
    private Context mContext;
    private List<ExpenseEntry> mExpenseList;
    private List<IncomeEntry> mIncomesList;

    public ExpensesAdapter(Context mContext, ItemClickListener itemClickListener) {
        this.mContext = mContext;
        this.mItemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public ExpensesAdapter.ExpensesViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_expense, viewGroup, false);
        return new ExpensesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ExpensesAdapter.ExpensesViewHolder holder, int position) {
        if (mExpenseList != null) {
            ExpenseEntry entry = mExpenseList.get(position);
            AppExecutors.getInstance().diskIO().execute(() -> {
                CategoryEntry categoryEntry = WalletDatabase.getInstance(mContext).categoryDao().loadCategoryById(entry.getCategoryId());
                new Handler(Looper.getMainLooper()).post(() -> {
                    // do something
                    holder.tvCategoryName.setText(categoryEntry.getName());
                    float cash = entry.getCash();
                    holder.tvCash.setText(String.format(Locale.getDefault(),"%,.2f", cash));
                    holder.tvDate.setText(entry.getDateF());
                });
            });
        } else if (mIncomesList != null) {
            IncomeEntry entry = mIncomesList.get(position);
            holder.tvDate.setText(entry.getDate());
            float cash = entry.getCash();
            holder.tvCash.setText(String.format(Locale.getDefault(),"%,.2f", cash));
            holder.tvCategoryName.setText(R.string.item_income_category);
        }
    }

    @Override
    public int getItemCount() {
        if (mExpenseList == null && mIncomesList != null) {
            if (mIncomesList.isEmpty()) {
                return 0;
            }
            else {
                return mIncomesList.size();
            }
        }

        if (mIncomesList == null && mExpenseList != null) {
            if (mExpenseList.isEmpty()) {
                return 0;
            }
            else {
                return mExpenseList.size();
            }
        }

        return 0;
    }

    public List<IncomeEntry> getmIncomesList() {
        return mIncomesList;
    }

    public void setmIncomesList(List<IncomeEntry> incomeList) {
        mIncomesList = incomeList;
        mExpenseList = null;
        notifyDataSetChanged();
    }

    public List<ExpenseEntry> getmExpenseList() {
        return mExpenseList;
    }

    public void setmExpenseList(List<ExpenseEntry> expenseList) {
        mExpenseList = expenseList;
        mIncomesList = null;
        notifyDataSetChanged();
    }


    public class ExpensesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.foreground)
        public FrameLayout mForeground;

        @BindView(R.id.background)
        public FrameLayout mBackground;

        @BindView(R.id.tv_bg_left)
        public TextView tvBgLeft;

        @BindView(R.id.tv_bg_right)
        public TextView tvBgRight;

        @BindView(R.id.tv_category_name)
        TextView tvCategoryName;

        @BindView(R.id.tv_cash)
        TextView tvCash;

        @BindView(R.id.tv_date)
        TextView tvDate;

        public ExpensesViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int elementId = 0;
            if (mExpenseList != null){
                elementId = mExpenseList.get(getAdapterPosition()).getId();
            } else if (mIncomesList != null)  {
                elementId = mIncomesList.get(getAdapterPosition()).getId();
            }
            mItemClickListener.onItemClickListener(v, elementId, "", 1);
        }
    }
}
