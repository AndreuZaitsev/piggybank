package acina.creative.ashwood.wallet.billing;

import java.util.ArrayList;

public class SKU {
    private static ArrayList<String> SKU_LIST = new ArrayList<>();

    // SKU(s) ID(s)
    private static final String MONTHLY_SUBSCRIPTION = "subscribe";


    public static ArrayList<String> getSkuList() {
        SKU_LIST.add(MONTHLY_SUBSCRIPTION);
        return SKU_LIST;
    }

    public static String getMonthlySubscription() {
        return MONTHLY_SUBSCRIPTION;
    }
}
