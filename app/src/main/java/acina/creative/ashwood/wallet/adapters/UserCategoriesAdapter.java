package acina.creative.ashwood.wallet.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import acina.creative.ashwood.wallet.R;
import acina.creative.ashwood.wallet.database.category.CategoryEntry;
import acina.creative.ashwood.wallet.model.MainActivity;
import acina.creative.ashwood.wallet.utils.StrUrltoInt;
import butterknife.BindView;
import butterknife.ButterKnife;

public class UserCategoriesAdapter extends RecyclerView.Adapter<UserCategoriesAdapter.CategoryViewHolder> {

    private Context mContext;
    private List<CategoryEntry> mCategoryEntries;

    /*
     * Constructor for the Adapter that initialize the Context
     * */
    public UserCategoriesAdapter(Context mContext) {
        this.mContext = mContext;

    }

    /*
     * When data changes, this method updates the list of categoryEntries
     * and notifies the adapter to use the new values on it
     * */
    public void setCategoryEntries(List<CategoryEntry> categoryEntryList ) {
        this.mCategoryEntries = categoryEntryList;
        notifyDataSetChanged();
    }

    /*
     * Get the list of categoryEntries
     * */
    public List<CategoryEntry> getCategoryEntries() {
        return mCategoryEntries;
    }


    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_category, viewGroup, false);

        return new CategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
        // Set values
        if (position == mCategoryEntries.size()) {
            holder.ivCategoryIcon.setImageResource(R.drawable.ic_add);
            holder.tvCategoryName.setText(R.string.add_category);
        } else {
            holder.ivCategoryIconWhite.setImageResource(StrUrltoInt.strToInt(mContext,mCategoryEntries.get(position).getUrl()));
            holder.ivCategoryIcon.setImageResource(StrUrltoInt.strToInt(mContext, mCategoryEntries.get(position).getUrl()));
            holder.tvCategoryName.setText(mCategoryEntries.get(position).getName() + "");
        }
    }

    @Override
    public int getItemCount() {
        if (mCategoryEntries == null) {
            return 0;
        }
        if (mContext instanceof MainActivity){
            return mCategoryEntries.size() + 1;
        } else {
            return mCategoryEntries.size();
        }
    }

    // Inner class for creating ViewHolders
    public class CategoryViewHolder extends RecyclerView.ViewHolder  {

        @BindView(R.id.iv_category_icon)
        ImageView ivCategoryIcon;

        @BindView(R.id.iv_category_icon_white)
        ImageView ivCategoryIconWhite;

        @BindView(R.id.tv_category_name)
        TextView tvCategoryName;

        private CategoryViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

    }
}
