package acina.creative.ashwood.wallet.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import acina.creative.ashwood.wallet.R;

public class AdMob {
    private InterstitialAd mInterstitialAd;
    private Context mContext;

    public AdMob(Context context) {
        this.mContext = context;
    }

    public void initAdmob(){
        MobileAds.initialize(mContext, mContext.getString(R.string.admob_key));
    }

    public void initInterstitialAd() {
        // Use an activity context to get the rewarded video instance.
        mInterstitialAd = new InterstitialAd(mContext);
        mInterstitialAd.setAdUnitId(mContext.getString(R.string.interstitial_key));
    }

    public void loadInterstitial() {
        mInterstitialAd.loadAd(new AdRequest.Builder()
                .addTestDevice("0705C13A0B0C334A1BCC68617932CFE0")
                .build());
    }

    public void setInterstitialAdListener(Class mClass, int requestCode, Activity activity) {
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
//                Toast.makeText(mContext, "onAdLoaded", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
//                Toast.makeText(mContext, "onAdFailedToLoad", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
//                Toast.makeText(mContext, "onAdOpened", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
//                Toast.makeText(mContext, "onAdLeftApplication", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when when the interstitial ad is closed.
//                Toast.makeText(mContext, "onAdClosed", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(mContext, mClass);
                activity.startActivityForResult(intent, requestCode);
            }
        });
    }

    public InterstitialAd getmInterstitialAd() {
        return mInterstitialAd;
    }


}
