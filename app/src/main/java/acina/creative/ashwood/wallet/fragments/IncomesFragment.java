package acina.creative.ashwood.wallet.fragments;

import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Canvas;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Objects;

import acina.creative.ashwood.wallet.R;
import acina.creative.ashwood.wallet.adapters.ExpensesAdapter;
import acina.creative.ashwood.wallet.database.WalletDatabase;
import acina.creative.ashwood.wallet.database.income.IncomeEntry;
import acina.creative.ashwood.wallet.interfaces.ItemClickListener;
import acina.creative.ashwood.wallet.utils.AppExecutors;
import acina.creative.ashwood.wallet.view_model.IncomesViewModel;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class IncomesFragment extends Fragment implements ItemClickListener {

    @BindView(R.id.incomes_recyclerview)
    RecyclerView mIncomesRecyclerView;

    @BindView(R.id.tv_empty)
    TextView tvEmpty;

    @BindView(R.id.cardview)
    CardView mCardView;

    private Unbinder unbinder;

    private ExpensesAdapter mAdapter;
    private WalletDatabase mDb;
    private IncomeEntry incomeEntry;


    @SuppressLint("StaticFieldLeak")
    private static IncomesFragment INSTANCE = null;


    public static IncomesFragment getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new IncomesFragment();
        }
        return INSTANCE;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_incomes, container, false);
        unbinder = ButterKnife.bind(this, view);


        mDb = WalletDatabase.getInstance(getContext());

        setupRecyclerView();

        setupViewModel();

        return view;
    }

    private void setupRecyclerView() {
        mIncomesRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mIncomesRecyclerView.setHasFixedSize(true);
        mIncomesRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mAdapter = new ExpensesAdapter(getContext(), this);
        mIncomesRecyclerView.setAdapter(mAdapter);

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull final RecyclerView.ViewHolder viewHolder, int direction) {
//                TextView tvBgLeft = ((ExpensesAdapter.ExpensesViewHolder) viewHolder).tvBgLeft;
//                TextView tvBgRight = ((ExpensesAdapter.ExpensesViewHolder) viewHolder).tvBgLeft;
                deleteIncomesEntry(viewHolder);
            }

            @Override
            public void onSelectedChanged(@Nullable RecyclerView.ViewHolder viewHolder, int actionState) {
                if (viewHolder != null) {
                    final View foregroundView = ((ExpensesAdapter.ExpensesViewHolder) viewHolder).mForeground;
                    getDefaultUIUtil().onSelected(foregroundView);
                }
            }

            @Override
            public void onChildDrawOver(@NonNull Canvas c, @NonNull RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                final View foregroundView = ((ExpensesAdapter.ExpensesViewHolder) viewHolder).mForeground;
                getDefaultUIUtil().onDrawOver(c, recyclerView, foregroundView, dX, dY,
                        actionState, isCurrentlyActive);
            }

            @Override
            public void clearView(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
                final View foregroundView = ((ExpensesAdapter.ExpensesViewHolder) viewHolder).mForeground;
                getDefaultUIUtil().clearView(foregroundView);
            }

            @Override
            public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                final View foregroundView = ((ExpensesAdapter.ExpensesViewHolder) viewHolder).mForeground;
                getDefaultUIUtil().onDraw(c, recyclerView, foregroundView, dX, dY,
                        actionState, isCurrentlyActive);
            }
        }).attachToRecyclerView(mIncomesRecyclerView);


    }

    private void deleteIncomesEntry(RecyclerView.ViewHolder viewHolder) {
        AppExecutors.getInstance().diskIO().execute(() -> {
            int position = viewHolder.getAdapterPosition();
            incomeEntry = mAdapter.getmIncomesList().get(position);
            mDb.incomeDao().deleteIncome(incomeEntry);
            Snackbar snackbar = Snackbar.make(Objects.requireNonNull(getView()), "Income Removed", Snackbar.LENGTH_LONG);
            snackbar.setAction("UNDO", v -> AppExecutors.getInstance().diskIO().execute(() -> mDb.incomeDao().insertIncome(incomeEntry)));
            snackbar.show();
        });


    }

    private void setupViewModel() {
        IncomesViewModel incomesViewModel = ViewModelProviders.of(this).get(IncomesViewModel.class);
        incomesViewModel.getIncomes().observe(this, list -> {
            if (list != null && !list.isEmpty()) {
                mAdapter.setmIncomesList(list);
                mCardView.setVisibility(View.VISIBLE);
                tvEmpty.setVisibility(View.GONE);
            } else {
                mCardView.setVisibility(View.GONE);
                tvEmpty.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onItemClickListener(View view, int itemId, String name, int url) {
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
