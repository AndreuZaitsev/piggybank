package acina.creative.ashwood.wallet.billing;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.Purchase;

import java.util.List;

public interface BillingUpdateListener {

    void onBillingClientSetupFinished();

    void onConsumeFinished(String token, @BillingClient.BillingResponse int result);

    void onPurchasesUpdated(List<Purchase> purchases);

}
