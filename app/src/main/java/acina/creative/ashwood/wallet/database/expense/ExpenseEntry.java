package acina.creative.ashwood.wallet.database.expense;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import acina.creative.ashwood.wallet.database.category.CategoryEntry;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(tableName = "expenses",
        foreignKeys = @ForeignKey(entity = CategoryEntry.class, parentColumns = "id", childColumns = "categoryId", onDelete = CASCADE),
        indices = {@Index("dateF")})
public class ExpenseEntry {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String dateF;
    private float cash;
    private int categoryId;

    public ExpenseEntry(int id, String dateF, float cash, int categoryId) {
        this.id = id;
        this.dateF = dateF;
        this.cash = cash;
        this.categoryId = categoryId;
    }

    @Ignore
    public ExpenseEntry(String dateF, float cash, int categoryId) {
        this.dateF = dateF;
        this.cash = cash;
        this.categoryId = categoryId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDateF() {
        return dateF;
    }

    public void setDateF(String dateF) {
        this.dateF = dateF;
    }

    public float getCash() {
        return cash;
    }

    public void setCash(float cash) {
        this.cash = cash;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }
}
