package acina.creative.ashwood.wallet.fragments;

import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Canvas;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;
import java.util.Objects;

import acina.creative.ashwood.wallet.R;
import acina.creative.ashwood.wallet.adapters.ExpensesAdapter;
import acina.creative.ashwood.wallet.database.WalletDatabase;
import acina.creative.ashwood.wallet.database.expense.ExpenseEntry;
import acina.creative.ashwood.wallet.interfaces.ItemClickListener;
import acina.creative.ashwood.wallet.utils.AppExecutors;
import acina.creative.ashwood.wallet.view_model.ExpensesViewModel;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ExpensesFragment extends Fragment implements ItemClickListener {

    @BindView(R.id.expenses_recyclerview)
    RecyclerView mExpensesRecyclerView;

    @BindView(R.id.tv_empty)
    TextView tvEmpty;

    @BindView(R.id.cardview)
    CardView mCardView;

    private Unbinder unbinder;

    private ExpensesAdapter mAdapter;
    private WalletDatabase mDb;
    private ExpenseEntry expenseEntry;


    @SuppressLint("StaticFieldLeak")
    private static ExpensesFragment INSTANCE = null;


    public static ExpensesFragment getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ExpensesFragment();
        }
        return INSTANCE;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_expenses, container, false);
        unbinder = ButterKnife.bind(this, view);


        mDb = WalletDatabase.getInstance(getContext());

        setupRecyclerView();

        setupViewModel();

        return view;
    }

    private void setupRecyclerView() {
        mExpensesRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mExpensesRecyclerView.setHasFixedSize(true);
        mExpensesRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mAdapter = new ExpensesAdapter(getContext(), this);
        mExpensesRecyclerView.setAdapter(mAdapter);

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull final RecyclerView.ViewHolder viewHolder, int direction) {
//                TextView tvBgLeft = ((ExpensesAdapter.ExpensesViewHolder) viewHolder).tvBgLeft;
//                TextView tvBgRight = ((ExpensesAdapter.ExpensesViewHolder) viewHolder).tvBgLeft;
                deleteExpenseEntry(viewHolder);
            }

            @Override
            public void onSelectedChanged(@Nullable RecyclerView.ViewHolder viewHolder, int actionState) {
                if (viewHolder != null) {
                    final View foregroundView = ((ExpensesAdapter.ExpensesViewHolder) viewHolder).mForeground;
                    getDefaultUIUtil().onSelected(foregroundView);
                }
            }

            @Override
            public void onChildDrawOver(@NonNull Canvas c, @NonNull RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                final View foregroundView = ((ExpensesAdapter.ExpensesViewHolder) viewHolder).mForeground;
                getDefaultUIUtil().onDrawOver(c, recyclerView, foregroundView, dX, dY,
                        actionState, isCurrentlyActive);
            }

            @Override
            public void clearView(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
                final View foregroundView = ((ExpensesAdapter.ExpensesViewHolder) viewHolder).mForeground;
                getDefaultUIUtil().clearView(foregroundView);
            }

            @Override
            public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                final View foregroundView = ((ExpensesAdapter.ExpensesViewHolder) viewHolder).mForeground;
                getDefaultUIUtil().onDraw(c, recyclerView, foregroundView, dX, dY,
                        actionState, isCurrentlyActive);
            }
        }).attachToRecyclerView(mExpensesRecyclerView);


    }

    private void deleteExpenseEntry(RecyclerView.ViewHolder viewHolder) {
        AppExecutors.getInstance().diskIO().execute(() -> {
            int position = viewHolder.getAdapterPosition();
            List<ExpenseEntry> records = mAdapter.getmExpenseList();
            expenseEntry = records.get(position);
            mDb.expenseDao().deleteExpense(expenseEntry);
            Snackbar snackbar = Snackbar.make(Objects.requireNonNull(getView()), "Expense Removed", Snackbar.LENGTH_LONG);
            snackbar.setAction("UNDO", v -> AppExecutors.getInstance().diskIO().execute(() -> mDb.expenseDao().insertExpenseEntry(expenseEntry)));
            snackbar.show();
        });


    }

    private void setupViewModel() {
        ExpensesViewModel expensesViewModel = ViewModelProviders.of(this).get(ExpensesViewModel.class);
        expensesViewModel.getExpensesByCategories().observe(this, list -> {
            if (list != null && !list.isEmpty()) {
                mCardView.setVisibility(View.VISIBLE);
                mAdapter.setmExpenseList(list);
                tvEmpty.setVisibility(View.GONE);
            } else {
                mCardView.setVisibility(View.GONE);
                tvEmpty.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onItemClickListener(View view, int itemId, String name, int url) {
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
