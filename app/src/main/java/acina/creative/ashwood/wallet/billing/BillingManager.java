package acina.creative.ashwood.wallet.billing;

import android.app.Activity;
import android.support.annotation.Nullable;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.ConsumeResponseListener;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

public class BillingManager implements PurchasesUpdatedListener {

    private Activity mActivity;
    private BillingClient mBillingClient;
    private boolean mIsServiceConnected  = false;
    int mBillingClientResponseCode;

    private BillingUpdateListener mBillingUpdateListener;

    public BillingManager(Activity mActivity, final BillingUpdateListener mBillingUpdateListener){
        this.mActivity = mActivity;
        this.mBillingUpdateListener = mBillingUpdateListener;

        mBillingClient = BillingClient.newBuilder(mActivity)
                .setListener(this)
                .build();

        startServiceConnection(mBillingUpdateListener::onBillingClientSetupFinished);
    }


    /*
     * method to start the connection between our app to google play
     * and call it in the constructor method to start the connection instantly.
     * */
    private void startServiceConnection(final Runnable runnable) {
        mBillingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(int responseCode) {

                if (responseCode == BillingClient.BillingResponse.OK) {
                    mIsServiceConnected = true;
                    if (runnable != null) {
                        runnable.run();
                    }
                }
                mBillingClientResponseCode = responseCode;
            }

            @Override
            public void onBillingServiceDisconnected() {
                Timber.w("billing: on Billing Service Disconnected");
                mIsServiceConnected = false;
            }
        });

    }


    @Override
    public void onPurchasesUpdated(int responseCode, @Nullable List<Purchase> purchases) {
        if (responseCode == BillingClient.BillingResponse.OK) {
            mBillingUpdateListener.onPurchasesUpdated(purchases);
        } else if (responseCode == BillingClient.BillingResponse.USER_CANCELED) {
            Timber.i("billing: onPurchasesUpdated() – user cancelled the purchase flow – skipping");
        } else {
            Timber.w("billing: onPurchasesUpdated() got unknown resultCode: %s", responseCode);
        }
    }

    /*
     * Our own retry policy if the connection is lost.
     * */
    private void executeServiceRequest(Runnable runnable) {
        if (mIsServiceConnected) {
            runnable.run();
        } else {
            // If the billing service disconnects, try to reconnect once.
            startServiceConnection(runnable);
        }
    }

    /*
     * When activity is created we should know which products are already bought by the user
     * and we should never bother them to buy the same product again,
     * so we will write a method which will return the already purchased item list
     * and we will call this method in the OnBillingClientSetup method
     * */
    public void queryPurchases() {
        Runnable queryToExecute = () -> {
            long time = System.currentTimeMillis();

            Purchase.PurchasesResult subscriptionResult = null;

            if (areSubscriptionsSupported()) {
                subscriptionResult = mBillingClient.queryPurchases(BillingClient.SkuType.SUBS);
                if (subscriptionResult.getResponseCode() == BillingClient.BillingResponse.OK) {
                    subscriptionResult.getPurchasesList().addAll(
                            subscriptionResult.getPurchasesList());
                } else {
                    // Handle any error response codes.
                    Timber.e("billing:  subscriptionResult.getResponseCode() != OK");
                }
            }  else {
                // Handle any other error response codes.
                Timber.e("billing:  Handle any other error response codes.");
            }
            onQueryPurchasesFinished(subscriptionResult);
        };

        executeServiceRequest(queryToExecute);
    }


    private void onQueryPurchasesFinished(Purchase.PurchasesResult result) {
        // Have we been disposed of in the meantime? If so, or bad result code, then quit
        if (mBillingClient == null || result.getResponseCode() != BillingClient.BillingResponse.OK) {
            Timber.w("billing: Billing client was null or result code (" + result.getResponseCode()
                    + ") was bad – quitting");
            return;
        }
        Timber.d("billing: Query inventory was successful.");
        // Update the UI and purchases inventory with new list of purchases
        // mPurchases.clear();
        onPurchasesUpdated(BillingClient.BillingResponse.OK, result.getPurchasesList());
    }

    // Check if subscription is available
    public boolean areSubscriptionsSupported() {
        int responseCode = mBillingClient.isFeatureSupported(BillingClient.FeatureType.SUBSCRIPTIONS);
        if (responseCode != BillingClient.BillingResponse.OK) {
            Timber.w("billing: areSubscriptionsSupported() got an error response: %s", responseCode);
        }
        return responseCode == BillingClient.BillingResponse.OK;
    }

    /*
     * Start selling a product, suppose we have a button
     * and when user clicks on this a payment flow UI provided by google play will pops up
     * */
    public void initiatePurchaseFlow(final String skuId, final ArrayList<String> oldSkus, final @BillingClient.SkuType String billingType) {
        Runnable purchaseFlowRequest = () -> {
            BillingFlowParams mParams = BillingFlowParams.newBuilder()
                    .setSku(skuId)
                    .setType(billingType)
                    .setOldSkus(oldSkus)
                    .build();
            mBillingClient.launchBillingFlow(mActivity, mParams);
        };

        executeServiceRequest(purchaseFlowRequest);
    }

    /*
     * Consume Product, once a product is bought it can never be bought again until it is consumed,
     * so we have to manually consume again to make this product available to buy again for this particular user.
     * */
    public void consumeAsync(final String purchaseToken) {
        // If we’ve already scheduled to consume this token – no action is needed (this could happen
        // if you received the token when querying purchases inside onReceive() and later from
        // onActivityResult()

        // Generating Consume Response listener
        final ConsumeResponseListener onConsumeListener = (responseCode, purchaseToken1) -> {
            // If billing service was disconnected, we try to reconnect 1 time
            // (feel free to introduce your retry policy here).
            mBillingUpdateListener.onConsumeFinished(purchaseToken1, responseCode);
        };

        // Creating a runnable from the request to use it inside our connection retry policy below
        Runnable consumeRequest = () -> {
            // Consume the purchase async
            mBillingClient.consumeAsync(purchaseToken, onConsumeListener);
        };

        executeServiceRequest(consumeRequest);
    }


    /*
     * There is another special thing u can do,
     * that is you can get a list of purchasable product and show them in an activity with the following method:
     * */
    public void querySkuDetailsAsync(final List<String> skuList, final SkuDetailsResponseListener skuListener) {
        // Create a runnable from the request to use inside the connection retry policy.
        Runnable queryRequest = () -> {
            // Create the SkuDetailParams object
            SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
            params.setSkusList(skuList).setType(BillingClient.SkuType.SUBS);
            // Run the query asynchronously.
            mBillingClient.querySkuDetailsAsync(params.build(), skuListener);
        };

        executeServiceRequest(queryRequest);
    }




}
