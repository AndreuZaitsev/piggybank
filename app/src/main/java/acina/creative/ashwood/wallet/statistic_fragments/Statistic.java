package acina.creative.ashwood.wallet.statistic_fragments;

import java.util.ArrayList;
import java.util.List;

import acina.creative.ashwood.wallet.database.expense.ExpenseEntry;

public class Statistic {
    public static final int MONTH = 1;
    public static final int DAY = 0;
    public static final int YEAR = 2;

    private static final String JANUARY = "01";
    private static final String FEBRUARY = "02";
    private static final String MARCH = "03";
    private static final String APRIL = "04";
    private static final String MAY = "05";
    private static final String JUNE = "06";
    private static final String JULY = "07";
    private static final String AUGUST = "08";
    private static final String SEPTEMBER = "09";
    private static final String OCTOBER = "10";
    private static final String NOVEMBER = "11";
    private static final String DECEMBER = "12";

    public static List<Object> groupByDate(List<ExpenseEntry> list, int period) {
        List<Object> objectList = new ArrayList<>();
        String objectDate = "";
        for (int i = 0; i < list.size(); i++) {
            String nextDate = list.get(i).getDateF();
            ExpenseEntry entry = list.get(i);

            if (!objectDate.equals(nextDate)) {
                objectDate = nextDate;
                if (period == DAY) {
                    objectList.add(objectDate);
                } else if (period == MONTH) {
                    objectList.add(monthly(objectDate));
                } else if (period == YEAR) {
                    objectList.add(objectDate.substring(6));
                } else {
                    objectList.add("Error");
                }
            }
            objectList.add(entry);
        }

        return objectList;
    }

    public static String monthly(String objectDate) {
        String month = objectDate.substring(3, 5);
        String year = objectDate.substring(6);
        switch (month) {
            case JANUARY:
                return "JANUARY, " + year;
            case FEBRUARY:
                return "FEBRUARY, " + year;
            case MARCH:
                return "MARCH, " + year;
            case APRIL:
                return "APRIL, " + year;
            case MAY:
                return "MAY, " + year;
            case JUNE:
                return "JUNE, " + year;
            case JULY:
                return "JULY, " + year;
            case AUGUST:
                return "AUGUST, " + year;
            case SEPTEMBER:
                return "SEPTEMBER, " + year;
            case OCTOBER:
                return "OCTOBER, " + year;
            case NOVEMBER:
                return "NOVEMBER, " + year;
            case DECEMBER:
                return "DECEMBER, " + year;
            default:
                return "MONTH, " + year;
        }
    }

}
