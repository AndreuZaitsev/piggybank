package acina.creative.ashwood.wallet.interfaces;

import android.view.View;

public interface ItemLongClickListener {
    void onClick(View view,int position);
    void onItemLongClickListener(View view, int itemId);

}
