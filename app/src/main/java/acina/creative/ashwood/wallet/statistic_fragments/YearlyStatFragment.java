package acina.creative.ashwood.wallet.statistic_fragments;


import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.arch.lifecycle.LiveData;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import acina.creative.ashwood.wallet.R;
import acina.creative.ashwood.wallet.adapters.PieLegendAdapter;
import acina.creative.ashwood.wallet.database.WalletDatabase;
import acina.creative.ashwood.wallet.database.expense.ExpenseEntry;
import acina.creative.ashwood.wallet.interfaces.ItemClickListener;
import acina.creative.ashwood.wallet.utils.DateFormat;
import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class YearlyStatFragment extends Fragment implements ItemClickListener {
    @BindColor(R.color.color_pie1)
    int color1;
    @BindColor(R.color.color_pie2)
    int color2;
    @BindColor(R.color.color_pie3)
    int color3;
    @BindColor(R.color.color_pie4)
    int color4;
    @BindColor(R.color.color_pie5)
    int color5;
    @BindColor(R.color.color_pie6)
    int color6;
    @BindColor(R.color.color_pie7)
    int color7;
    @BindColor(R.color.color_pie8)
    int color8;
    @BindColor(R.color.color_pie9)
    int color9;
    @BindColor(R.color.color_pie10)
    int color10;
    @BindColor(R.color.color_pie11)
    int color11;

    @BindView(R.id.tv_date)
    TextView tvDate;

    @BindView(R.id.pie_chart_year)
    PieChart mPieChart;

    @BindView(R.id.yearly_recyclerview)
    RecyclerView mExpensesRecyclerView;

    @BindView(R.id.tv_empty)
    TextView tvEmpty;

    @BindView(R.id.cardview)
    CardView mCardView;

    @BindView(R.id.cardview2)
    CardView mCardView2;

    private List<PieEntry> pieEntries = new ArrayList<>();
    private PieDataSet pieDataSet;
    private WalletDatabase mDb;
    private String mDate;
    private PieData pieData;

    LiveData<List<ExpenseEntry>> expenseListLiveData;

    private Unbinder unbinder;

    private PieLegendAdapter mAdapter;

    @SuppressLint("StaticFieldLeak")
    private static YearlyStatFragment INSTANCE = null;

    public static YearlyStatFragment getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new YearlyStatFragment();
        }
        return INSTANCE;
    }

    public YearlyStatFragment() {
        // Required empty public constructor

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_yearly_stat, container, false);

        unbinder = ButterKnife.bind(this, view);

        mDb = WalletDatabase.getInstance(getContext());

        setupRecyclerView();

        setupPieChart();

        mDate = DateFormat.dateToString(new Date());
        updateData(mDate);

        return view;
    }

    private void setupRecyclerView() {
        mExpensesRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mExpensesRecyclerView.setHasFixedSize(true);

        mAdapter = new PieLegendAdapter(getContext(), this);
        mExpensesRecyclerView.setAdapter(mAdapter);

    }

    private void updateData(String date) {
        tvDate.setText(date.substring(6));
        pieEntries.clear();
        Timber.d("updating query");
        expenseListLiveData = mDb.expenseDao().loadSumExpensesByCategoriesByYear(date.substring(6));
        expenseListLiveData.observe(this, list-> updating(list));
    }

    private void updating(List<ExpenseEntry> list) {
        if (list != null && !list.isEmpty()) {
            for (ExpenseEntry entry : list) {
                float cash = entry.getCash();
                pieEntries.add(new PieEntry(cash, "name", entry));
            }
            Timber.d("updating if");
            mPieChart.setCenterText(String.valueOf(pieData.getYValueSum()));
            mAdapter.setPieData(pieData);
            mPieChart.notifyDataSetChanged();
//            mPieChart.invalidate();
            mCardView.setVisibility(View.VISIBLE);
            mCardView2.setVisibility(View.VISIBLE);
            tvEmpty.setVisibility(View.GONE);
        } else {
            Timber.d("updating else");
            mCardView.setVisibility(View.GONE);
            mCardView2.setVisibility(View.GONE);
            tvEmpty.setVisibility(View.VISIBLE);
        }
    }

    private void setupPieChart() {
        mPieChart.setUsePercentValues(true); // percent instead real values
        mPieChart.getDescription().setEnabled(false);

        mPieChart.setExtraOffsets(25, 15, 25, 20);
        mPieChart.setDragDecelerationFrictionCoef(0.95f);

        mPieChart.setCenterTextSize(18);

        mPieChart.setDrawHoleEnabled(true);
        mPieChart.setHoleColor(Color.WHITE);
        mPieChart.setTransparentCircleColor(Color.TRANSPARENT);
        mPieChart.setHoleRadius(70f);

        mPieChart.setDrawCenterText(true);

        // enable rotation of the chart by touch
        mPieChart.setRotationEnabled(false);

        mPieChart.getLegend().setEnabled(false);

        // entry label styling
        mPieChart.setDrawEntryLabels(false);
        mPieChart.setEntryLabelColor(Color.BLACK);
        mPieChart.setEntryLabelTextSize(12f);

        pieDataSet = new PieDataSet(pieEntries, "Expenses");
        pieDataSet.setDrawIcons(false);
        pieDataSet.setSelectionShift(0);

        ArrayList<Integer> colors = new ArrayList<>();
        colors.add(color1);
        colors.add(color2);
        colors.add(color3);
        colors.add(color4);
        colors.add(color5);
        colors.add(color6);
        colors.add(color7);
        colors.add(color8);
        colors.add(color9);
        colors.add(color10);
        colors.add(color11);
        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);
        pieDataSet.setColors(colors);

        pieDataSet.setXValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
        pieDataSet.setDrawValues(true);
        pieDataSet.setValueLineColor(R.color.color_pie_value);
        pieDataSet.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
        pieDataSet.setValueLinePart1OffsetPercentage(80.f);
        pieDataSet.setValueLinePart1Length(0.28f);
        pieDataSet.setValueLinePart2Length(0.2f);

        pieData = new PieData(pieDataSet);
        pieData.setValueTextSize(11);
        pieData.setValueTextColor(R.color.color_pie_value);
        pieData.setValueFormatter(new PercentFormatter());
        mPieChart.setData(pieData);
        mPieChart.highlightValues(null);
        mPieChart.animateY(1000, Easing.EaseInOutCubic);


    }

    @OnClick(R.id.select_date)
    public void selectDate(View view) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getActivity().getSupportFragmentManager(), "datePicker");
    }

    @Override
    public void onItemClickListener(View view, int itemId, String name, int url) {
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        public String mDate;

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            DatePickerDialog dialog = new DatePickerDialog(getActivity(), android.R.style.Theme_Holo_Light_Dialog, this, year, month, day);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setTitle("Select the year");
            dialog.getDatePicker().findViewById(getResources().getIdentifier("day", "id", "android")).setVisibility(View.GONE);
            dialog.getDatePicker().findViewById(getResources().getIdentifier("month", "id", "android")).setVisibility(View.GONE);

            return dialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            // dd-mm-yyyy
            mDate = "01-" + "01-" + year;
            Timber.d(mDate);

            YearlyStatFragment.getInstance().updateData(mDate);

        }
    }

}
