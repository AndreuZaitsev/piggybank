package acina.creative.ashwood.wallet.model;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.ads.InterstitialAd;

import acina.creative.ashwood.wallet.R;
import acina.creative.ashwood.wallet.billing.BillingActivity;
import acina.creative.ashwood.wallet.billing.BillingPreference;
import acina.creative.ashwood.wallet.fragments.AboutFragment;
import acina.creative.ashwood.wallet.fragments.ExpensesFragment;
import acina.creative.ashwood.wallet.fragments.FaqFragment;
import acina.creative.ashwood.wallet.fragments.IncomesFragment;
import acina.creative.ashwood.wallet.fragments.MainFragment;
import acina.creative.ashwood.wallet.utils.AdMob;
import acina.creative.ashwood.wallet.utils.Common;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

public class MainActivity extends AppCompatActivity {
    private InterstitialAd mInterstitialAd;
    private AdMob mAdMob;

    // Annotate fields with @BindView and a view ID for Butter Knife
    @BindView(R.id.main_toolbar)
    Toolbar mToolbar;

    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;

    @BindView(R.id.nav_view)
    NavigationView mNavigationView;

    private int mClickedNavItem = 0;
    private Fragment mFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Butter Knife
        ButterKnife.bind(this);
        setAdMob();

        setToolbar();

        toggleNavDrawer();

        navigationViewOnClick();

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, MainFragment.getInstance()).commit();
            mNavigationView.setCheckedItem(R.id.nav_main);
        }

    }

    private void setAdMob() {
        mAdMob = new AdMob(this);
        mAdMob.initInterstitialAd();
        mInterstitialAd = mAdMob.getmInterstitialAd();
        if (!BillingPreference.isSubscribed(this)) {
            mAdMob.loadInterstitial();
        }
    }

    private void toggleNavDrawer() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState();
    }

    private void setToolbar() {
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void navigationViewOnClick() {
        mNavigationView.setNavigationItemSelectedListener(menuItem -> {
            // set item as selected to persist highlight
            menuItem.setChecked(true);
            // close drawer when item is tapped
            mDrawerLayout.closeDrawers();
            // handle an item selection
            switch (menuItem.getItemId()) {
                case R.id.nav_main:
                    mClickedNavItem = R.id.nav_main;
                    mFragment = MainFragment.getInstance();
                    break;
                case R.id.nav_expenses:
                    mClickedNavItem = R.id.nav_expenses;
                    mFragment = ExpensesFragment.getInstance();
                    break;
                case R.id.nav_incomes:
                    mClickedNavItem = R.id.nav_incomes;
                    mFragment = IncomesFragment.getInstance();
                    break;
                case R.id.nav_exit:
                    mClickedNavItem = R.id.nav_exit;
                    break;
                case R.id.nav_about:
                    mClickedNavItem = R.id.nav_about;
                    mFragment = AboutFragment.getInstance();
                    break;
                case R.id.nav_faq:
                    mClickedNavItem = R.id.nav_faq;
                    mFragment = FaqFragment.getInstance();
                    break;
                case R.id.nav_billing:
                    mClickedNavItem = R.id.nav_billing;
                    break;

            }
            return true;
        });

        mDrawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View view, float v) {

            }

            @Override
            public void onDrawerOpened(@NonNull View view) {

            }

            @Override
            public void onDrawerClosed(@NonNull View view) {
                switch (mClickedNavItem) {
                    case R.id.nav_exit:
                        closeApp();
                        break;
                    case R.id.nav_billing:
                        Intent intent = new Intent(MainActivity.this, BillingActivity.class);
                        startActivity(intent);
                        break;
                    default:
                        if (mFragment != null) {
                            getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, mFragment).commit();
                        }
                        break;
                }
            }

            @Override
            public void onDrawerStateChanged(int i) {

            }
        });
    }

    private void closeApp() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Close the app ?")
                .setPositiveButton("YES", (dialogInterface, i) -> {
                    moveTaskToBack(true);
                    android.os.Process.killProcess(android.os.Process.myPid());
                    System.exit(1);
                })
                .setNegativeButton("cancel", ((dialogInterface, i) -> Timber.i("")));
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Timber.d("resume");
        if (!mInterstitialAd.isLoaded()) {
            if (!BillingPreference.isSubscribed(this)) {
                mAdMob.loadInterstitial();
            }
        }
    }

    @OnClick(R.id.tv_statistic)
    public void openStatistic() {
        if (mInterstitialAd.isLoaded()) {
            mAdMob.setInterstitialAdListener(StatisticActivity.class, Common.REQUEST_CODE, this);
            mInterstitialAd.show();
        } else {
            Intent intent = new Intent(MainActivity.this, StatisticActivity.class);
            startActivityForResult(intent, Common.REQUEST_CODE);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Common.REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                String editIntent = data != null ? data.getStringExtra("EDIT_INTENT") : "default";
                Timber.d("key: %s", editIntent);
                switch (editIntent) {
                    case "incomes":
                        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, IncomesFragment.getInstance()).commit();
                        mNavigationView.setCheckedItem(R.id.nav_incomes);
                        break;
                    case "expenses":
                        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, ExpensesFragment.getInstance()).commit();
                        mNavigationView.setCheckedItem(R.id.nav_expenses);
                        break;
                    default:
                        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, MainFragment.getInstance()).commit();
                        mNavigationView.setCheckedItem(R.id.nav_main);
                        break;
                }
            }
        }
    }
}
