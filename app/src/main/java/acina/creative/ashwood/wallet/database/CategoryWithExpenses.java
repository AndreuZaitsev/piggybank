package acina.creative.ashwood.wallet.database;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Relation;

import java.util.List;

import acina.creative.ashwood.wallet.database.category.CategoryEntry;
import acina.creative.ashwood.wallet.database.expense.ExpenseEntry;

public class CategoryWithExpenses {

    @Embedded public CategoryEntry mCategoryEntry;

    @Relation(parentColumn = "id", entityColumn = "categoryId")
    public List<ExpenseEntry> mExpenseList;



}
