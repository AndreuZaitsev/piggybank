package acina.creative.ashwood.wallet.statistic_fragments;


import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import acina.creative.ashwood.wallet.R;
import acina.creative.ashwood.wallet.adapters.StatisticAdapter;
import acina.creative.ashwood.wallet.interfaces.ItemClickListener;
import acina.creative.ashwood.wallet.view_model.DailyStatViewModel;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;



/**
 * A simple {@link Fragment} subclass.
 */
public class DailyStatFragment extends Fragment implements ItemClickListener {

    @BindView(R.id.daily_recyclerview)
    RecyclerView mExpensesRecyclerView;

    @BindView(R.id.tv_empty)
    TextView tvEmpty;

    @BindView(R.id.cardview)
    CardView cardView;

    private Unbinder unbinder;

    private StatisticAdapter mAdapter;

    @SuppressLint("StaticFieldLeak")
    private static DailyStatFragment INSTANCE = null;

    public static DailyStatFragment getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new DailyStatFragment();
        }
        return INSTANCE;
    }

    public DailyStatFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_daily_stat, container, false);

        unbinder = ButterKnife.bind(this, view);

        setupRecyclerView();

        setupViewModel();

        return view;
    }

    private void setupRecyclerView() {
        mExpensesRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mExpensesRecyclerView.setHasFixedSize(true);

        mAdapter = new StatisticAdapter(getContext(), this);
        mExpensesRecyclerView.setAdapter(mAdapter);

    }

    private void setupViewModel(){
        DailyStatViewModel dailyStatViewModel = ViewModelProviders.of(this).get(DailyStatViewModel.class);
        dailyStatViewModel.getExpensesByCategories().observe(this, list -> {
            if (list != null && !list.isEmpty()) {
                mAdapter.setExpenseEntries(Statistic.groupByDate(list, Statistic.DAY));
                tvEmpty.setVisibility(View.GONE);
                cardView.setVisibility(View.VISIBLE);
            } else {
                cardView.setVisibility(View.GONE);
                tvEmpty.setVisibility(View.VISIBLE);

            }
        });
    }



    @Override
    public void onItemClickListener(View view, int itemId, String name, int url) {
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
