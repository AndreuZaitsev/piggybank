package acina.creative.ashwood.wallet.fragments;


import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.InterstitialAd;

import java.util.Date;
import java.util.Objects;

import acina.creative.ashwood.wallet.R;
import acina.creative.ashwood.wallet.adapters.UserCategoriesAdapter;
import acina.creative.ashwood.wallet.billing.BillingPreference;
import acina.creative.ashwood.wallet.calculator.MathFunctions;
import acina.creative.ashwood.wallet.calculator.MathOperations;
import acina.creative.ashwood.wallet.calculator.MathSigns;
import acina.creative.ashwood.wallet.database.WalletDatabase;
import acina.creative.ashwood.wallet.database.category.CategoryEntry;
import acina.creative.ashwood.wallet.database.expense.ExpenseEntry;
import acina.creative.ashwood.wallet.interfaces.ItemLongClickListener;
import acina.creative.ashwood.wallet.model.AddCategoryActivity;
import acina.creative.ashwood.wallet.model.ChangeBudgetActivity;
import acina.creative.ashwood.wallet.utils.AdMob;
import acina.creative.ashwood.wallet.utils.AppExecutors;
import acina.creative.ashwood.wallet.utils.CirclePagerIndicatorDecoration;
import acina.creative.ashwood.wallet.utils.Common;
import acina.creative.ashwood.wallet.utils.DateFormat;
import acina.creative.ashwood.wallet.utils.PickCategoryAnimation;
import acina.creative.ashwood.wallet.utils.RecyclerTouchListener;
import acina.creative.ashwood.wallet.view_model.MainViewModel;
import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import timber.log.Timber;

import static acina.creative.ashwood.wallet.calculator.MathOperations.ADDITION;
import static acina.creative.ashwood.wallet.calculator.MathOperations.SUBTRACT;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment {
    // AdMob
    private InterstitialAd mInterstitialAd;
    private AdMob mAdMob;


    @BindView(R.id.category_recyclerview)
    RecyclerView mCategoryRecyclerView;

    @BindView(R.id.tv_budget)
    TextView tvBudget;

    @BindView(R.id.editCash)
    EditText mEditCash;

    @BindView(R.id.income)
    FloatingActionButton mIncomeButton;

    @BindDrawable(R.drawable.bg_checked)
    Drawable mCheckedIcon;

    private Unbinder unbinder;

    private UserCategoriesAdapter mAdapter;
    private WalletDatabase mDb;

    private final String KEY_CATEGORY = "checked_category_key";
    private final int NO_CATEGORY = -1;
    private Bundle mCheckedCategory = new Bundle();
    private ImageView ivIconWhite;

    private float mBalance, mTotalIncomes, mTotalExpenses;

    private static final String ARITHMETIC_OPERATION_KEY = "ARITHMETIC_OPERATION_KEY";
    private float mValueOne, mValueTwo;
    private Bundle mMathOperationBundle = new Bundle();

    private CategoryEntry categoryEntry;
    private ExpenseEntry expenseEntry;
    private long lastId;

    private PickCategoryAnimation mAnim;

    @SuppressLint("StaticFieldLeak")
    private static MainFragment INSTANCE = null;


    public static MainFragment getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new MainFragment();
        }
        return INSTANCE;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        unbinder = ButterKnife.bind(this, view);

        setAdMob();

        setupRecyclerView();

        mDb = WalletDatabase.getInstance(getContext());

        setupViewModel();

        mCheckedCategory.putInt(KEY_CATEGORY, NO_CATEGORY);

        return view;
    }

    @SuppressLint("LogNotTimber")
    private void setupViewModel() {
        MainViewModel mainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        // get a list of user's categories
        mainViewModel.getCategoryEntryList().observe(this, categoryEntries -> mAdapter.setCategoryEntries(categoryEntries));
        // Update balance with income
        mainViewModel.getIncomesCash().observe(this, cash -> {
            mTotalIncomes = cash != null ? cash.getCash() : 0;
            mBalance = mTotalIncomes - mTotalExpenses;
            tvBudget.setText(String.valueOf(mBalance));
        });
        // Update balance with expense
        mainViewModel.getExpensesCash().observe(this, cash -> {
            mTotalExpenses = cash != null ? cash.getCash() : 0;
            mBalance = mTotalIncomes - mTotalExpenses;
            tvBudget.setText(String.valueOf(mBalance));
        });

    }


    private void setAdMob() {
        mAdMob = new AdMob(getActivity());
        mAdMob.initInterstitialAd();
        mInterstitialAd = mAdMob.getmInterstitialAd();
        if (!BillingPreference.isSubscribed(Objects.requireNonNull(getContext()))) {
            mAdMob.loadInterstitial();
        }
    }

    private void setupRecyclerView() {
        mCategoryRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2, GridLayoutManager.HORIZONTAL, false));
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView

        mAdapter = new UserCategoriesAdapter(getContext());
        mCategoryRecyclerView.setAdapter(mAdapter);

        SnapHelper helper = new LinearSnapHelper();
        helper.attachToRecyclerView(mCategoryRecyclerView);
        mCategoryRecyclerView.addItemDecoration(new CirclePagerIndicatorDecoration(6));

        mCategoryRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), mCategoryRecyclerView, new ItemLongClickListener() {
            @Override
            public void onClick(View view, int itemId) {
                // The last item in the recycler view is for adding a new category
                if (itemId == mAdapter.getItemCount() - 1) {
                    intentAddCategoryWithAnim(view);
                } else {
                    pickCategory(view, itemId);
                }
            }

            @Override
            public void onItemLongClickListener(View view, int itemId) {
                deleteCategory(itemId);
            }
        }));

    }

    private void intentAddCategoryWithAnim(View view) {
        ObjectAnimator.ofFloat(view.findViewById(R.id.iv_category_icon), View.ROTATION, 0f, 360f).setDuration(450).start();
        final Handler handler = new Handler();
        handler.postDelayed(() -> {
            if (mInterstitialAd.isLoaded()) {
                mAdMob.setInterstitialAdListener(AddCategoryActivity.class, Common.REQUEST_CODE, getActivity());
                mInterstitialAd.show();
            } else {
                clearAllSelection();
                Intent intent = new Intent(getActivity(), AddCategoryActivity.class);
                startActivity(intent);
            }
        }, 450);
    }

    private void deleteCategory(int itemId) {
        if (itemId != mAdapter.getItemCount() - 1) {
            AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getActivity()));
            builder.setMessage("Deletion of a category will delete the all previous expenses linked to it")
                    .setTitle("Delete Category")
                    .setPositiveButton("OK", (dialogInterface, i) -> AppExecutors.getInstance().diskIO().execute(() -> {

                        categoryEntry = mAdapter.getCategoryEntries().get(itemId);
                        mDb.categoryDao().deleteCategory(categoryEntry);

                        Snackbar snackbar = Snackbar.make(Objects.requireNonNull(getView()), "Income Removed", Snackbar.LENGTH_LONG);
                        snackbar.setAction("UNDO", v -> AppExecutors.getInstance().diskIO().execute(() -> mDb.categoryDao().insertCategory(categoryEntry)));
                        snackbar.show();
                    }))
                    .setNegativeButton("cancel", ((dialogInterface, i) -> Timber.d("canceled")));
            AlertDialog dialog = builder.create();
            dialog.show();
        }


    }

    private void pickCategory(View view, int itemId) {
        CategoryEntry categoryEntry = mAdapter.getCategoryEntries().get(itemId);
        int check = mCheckedCategory.getInt(KEY_CATEGORY);

        if (check != categoryEntry.getId() && ivIconWhite != null) {
            // Uncheck previous
            mAnim.setAnimation(60, null);
            mCheckedCategory.putInt(KEY_CATEGORY, NO_CATEGORY);
        }

        ivIconWhite = view.findViewById(R.id.iv_category_icon_white);
        RelativeLayout rootIvIcon = view.findViewById(R.id.root_iv_category_icon);
        mAnim = new PickCategoryAnimation(getContext(), rootIvIcon, ivIconWhite);

        check = mCheckedCategory.getInt(KEY_CATEGORY);
        if (check == NO_CATEGORY) {
            // Check
            mCheckedCategory.putInt(KEY_CATEGORY, categoryEntry.getId());
            mAnim.setAnimation(30, mCheckedIcon);
//            int finalRadius = (int)Math.hypot(rootIvIcon.getWidth()/2, rootIvIcon.getHeight()/2);
//            Animator anim = ViewAnimationUtils.createCircularReveal(rootIvIcon, rootIvIcon.getWidth()/2, rootIvIcon.getHeight()/2, 0, finalRadius);
//            rootIvIcon.setBackground(mCheckedIcon);
//            anim.setDuration(100);
//            anim.start();
        } else {
            // Uncheck
            mCheckedCategory.putInt(KEY_CATEGORY, NO_CATEGORY);
            mAnim.setAnimation(60, null);
        }
    }

    private void clearAllSelection() {
        mCheckedCategory.putInt(KEY_CATEGORY, NO_CATEGORY);
        mEditCash.setText(null);
        if (mAnim != null) {
            mAnim.setAnimation(60, null);
        }
        mValueOne = 0;
        mValueTwo = 0;
    }

    @OnClick(R.id.save_income)
    public void saveExpense() {
        try {
            if (mCheckedCategory.getInt(KEY_CATEGORY) == NO_CATEGORY) throw new Exception("Choose a category");
            float cash = Float.parseFloat(mEditCash.getText().toString());
            if (cash == 0) throw new Exception("Use a value greater than zero");
            String date = DateFormat.dateToString(new Date());
            //String date = "09-02-2020";
            expenseEntry = new ExpenseEntry(date, MathFunctions.round(cash,2), mCheckedCategory.getInt(KEY_CATEGORY));
            AppExecutors.getInstance().diskIO().execute(() -> {
                lastId = mDb.expenseDao().insertExpenseEntry(expenseEntry);
                // Undo inserting
                Snackbar snackbar = Snackbar.make(Objects.requireNonNull(getView()), "Expense Added", Snackbar.LENGTH_SHORT);
                snackbar.setAction("UNDO", v -> AppExecutors.getInstance().diskIO().execute(() -> mDb.expenseDao().deleteExpenseById((int) lastId)));
                snackbar.show();
            });
            Objects.requireNonNull(getActivity()).runOnUiThread(this::clearAllSelection);
        } catch (NumberFormatException e) {
            Toast.makeText(getContext(), "Wrong value", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.income)
    public void incomeOnClick(View view) {
        if (mInterstitialAd.isLoaded()) {
            mAdMob.setInterstitialAdListener(ChangeBudgetActivity.class, Common.REQUEST_CODE, getActivity());
            mInterstitialAd.show();
        } else {
            Intent intent = new Intent(getActivity(), ChangeBudgetActivity.class);
            animateFabOnClick(view, intent);
        }
    }

    private void animateFabOnClick(View view, Intent intent) {
        ObjectAnimator.ofFloat(view, View.ROTATION, 0f, 360f).setDuration(450).start();
        final Handler handler = new Handler();
        handler.postDelayed(() -> {
            clearAllSelection();
            getActivity().startActivityForResult(intent, Common.REQUEST_CODE);
        }, 450);
    }

    /*
     * LifeCycle Events
     * */
    @Override
    public void onResume() {
        super.onResume();
        if (!mInterstitialAd.isLoaded()) {
            if (!BillingPreference.isSubscribed(Objects.requireNonNull(getContext()))) {
                mAdMob.loadInterstitial();
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        clearAllSelection();
        unbinder.unbind();
    }

    /*
     * Sets the CALCULATOR buttons click listener
     * */
    @SuppressLint("SetTextI18n")
    @OnClick({R.id.calc_0, R.id.calc_1, R.id.calc_2, R.id.calc_3,
            R.id.calc_4, R.id.calc_5, R.id.calc_6,
            R.id.calc_7, R.id.calc_8, R.id.calc_9,
            R.id.calc_dot, R.id.calc_minus, R.id.calc_plus,
            R.id.calc_equal, R.id.calc_delete, R.id.calc_clear})
    public void calcBtnOnClick(Button btn) {

        String sign = btn.getText().toString();

        if (btn.getId() == R.id.calc_delete) {
            calcDeleteLast();

        } else if (btn.getId() == R.id.calc_clear) {
            mEditCash.setText("");
            mValueOne = 0;
            mValueTwo = 0;

        } else if (sign.equals(MathSigns.EQUAL)) {
            calcEqual();

        } else if (sign.equals(MathSigns.PLUS)) {
            calcMathSign(sign);

        } else if (sign.equals(MathSigns.MINUS)) {
            calcMathSign(sign);

        } else if (sign.equals(MathSigns.DOT)) {
            calcMathSign(sign);

        } else {
            // That's mean user has clicked a number
            mEditCash.setText(mEditCash.getText() + sign);
        }
    }

    private void calcEqual() {
        String text = String.valueOf(mEditCash.getText());
        if (text.length() > 0) {
            String lastChar = String.valueOf(mEditCash.getText().charAt(text.length() - 1));
            if (!lastChar.equals(MathSigns.PLUS) && !lastChar.equals(MathSigns.PLUS) && !lastChar.equals(MathSigns.DOT)) {
                String[] result = text.split("(?<=[-+*/])|(?=[-+*/])");
                for (int i = 0; i < result.length; i++) {
                    String val = result[i];
                    switch (val) {
                        case MathSigns.PLUS:
                            mMathOperationBundle.putSerializable(ARITHMETIC_OPERATION_KEY, ADDITION);
                            break;
                        case MathSigns.MINUS:
                            mMathOperationBundle.putSerializable(ARITHMETIC_OPERATION_KEY, SUBTRACT);
                            break;
                        default:
                            if (i == 0) {
                                mValueOne = Float.parseFloat(val);
                            } else {
                                mValueTwo = Float.parseFloat(val);
                                doMath();
                            }
                            break;
                    }
                }
                String sum = String.valueOf(MathFunctions.round(mValueOne, 2));
                mEditCash.setText(sum);
            }
        }
    }

    // Define a math sign
    @SuppressLint("SetTextI18n")
    private void calcMathSign(String sign) {
        int length = mEditCash.getText().length();
        String value = mEditCash.getText().toString();
        if (length > 0) {
            String lastChar = String.valueOf(mEditCash.getText().charAt(length - 1));
            if (!lastChar.equals(MathSigns.PLUS) && !lastChar.equals(MathSigns.MINUS) && !lastChar.equals(MathSigns.DOT)) {
                mEditCash.setText(value + sign);
            }
        }
    }

    private void calcDeleteLast() {
        int length = mEditCash.getText().length();
        if (length > 0) {
            mEditCash.getText().delete(length - 1, length);
        }
    }

    private void doMath() {
        MathOperations mathOperationType = (MathOperations) mMathOperationBundle.get(ARITHMETIC_OPERATION_KEY);
        if (mathOperationType != null) {
            switch (mathOperationType) {
                case ADDITION:
                    mValueOne += mValueTwo;
                    break;
                case SUBTRACT:
                    mValueOne -= mValueTwo;
            }
        }
    }

}
