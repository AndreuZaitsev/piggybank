package acina.creative.ashwood.wallet.model;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.constraint.Group;
import android.support.v7.app.AppCompatActivity;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.view.View;

import com.android.billingclient.api.Purchase;
import com.google.android.gms.ads.InterstitialAd;

import java.util.ArrayList;
import java.util.List;

import acina.creative.ashwood.wallet.R;
import acina.creative.ashwood.wallet.billing.BillingActivity;
import acina.creative.ashwood.wallet.billing.BillingManager;
import acina.creative.ashwood.wallet.billing.BillingPreference;
import acina.creative.ashwood.wallet.billing.BillingUpdateListener;
import acina.creative.ashwood.wallet.utils.AdMob;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;


public class SplashActivity extends AppCompatActivity {
    private InterstitialAd mInterstitialAd;
    private AdMob mAdMob;

    private BillingManager mBillingManager;
    private ArrayList<String> mSkuIdList;

    @BindView(R.id.logo_group)
    Group mLogoGroup;

    @BindView(R.id.sub_group)
    Group mSubscribeGroup;

    private Bundle mBundle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        setupWindowAnimation();

        setContentView(R.layout.activity_splash);

        ButterKnife.bind(this);

        // Init timber logger
        Timber.plant(new Timber.DebugTree());

        if (!BillingPreference.isSubscribed(this)) {
            setAdMob();
        }
    }

    private void setupWindowAnimation() {
        TransitionInflater inflater = TransitionInflater.from(this);
        Transition transition = inflater.inflateTransition(R.transition.explode);
        transition.excludeTarget(android.R.id.statusBarBackground, true);
        transition.excludeTarget(android.R.id.navigationBarBackground, true);
        getWindow().setExitTransition(transition);
    }

    private void initBillingManager() {
        mBillingManager = new BillingManager(this, new BillingUpdateListener() {
            @Override
            public void onBillingClientSetupFinished() {
                Timber.i("billing: onBillingClientSetupFinished");
                mBillingManager.queryPurchases();
            }

            @Override
            public void onConsumeFinished(String token, int result) {
                Timber.i("billing: onConsumeFinished");
            }

            @Override
            public void onPurchasesUpdated(List<Purchase> purchases) {
                Timber.d("billing: onPurchasesUpdated");
                Purchase purchase;
                try {
                    purchase = purchases.get(0);
                    if (purchase != null) {
                        Timber.i("billing: purchase: %s", purchase);
                        updateUI(purchase);
                    }
                } catch (IndexOutOfBoundsException e) {
                    Timber.e(e, "billing: Error in purchase.get(0)");
                }

            }
        });
    }

    private void setAdMob() {
        mAdMob = new AdMob(this);
        mAdMob.initAdmob();
        mAdMob.initInterstitialAd();
        mInterstitialAd = mAdMob.getmInterstitialAd();
        mAdMob.setInterstitialAdListener(MainActivity.class, 9999, this);
        if (!BillingPreference.isSubscribed(this)) {
//            mAdMob.loadInterstitial();
        }
    }

    @OnClick(R.id.btnFree)
    public void chooseFree() {
        startActivity(new Intent(SplashActivity.this, MainActivity.class));
    }

    @OnClick(R.id.btnSub)
    public void chooseSubscription() {
        Intent intent = new Intent(SplashActivity.this, BillingActivity.class);
//        Bundle bundle = ActivityOptions.makeSceneTransitionAnimation(this, null).toBundle();
        this.startActivity(intent);

    }

    @Override
    protected void onResume() {
        super.onResume();

        // Check if the user is subscribed
        initBillingManager();
        if (BillingPreference.isSubscribed(this)) {
            mSubscribeGroup.setVisibility(View.GONE);
            new Handler().postDelayed(() -> {
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
            }, 1000);
        } else {
            mSubscribeGroup.setVisibility(View.VISIBLE);
        }

    }

    private void updateUI(Purchase purchase) {
        BillingPreference.setmIsSubscribedPref(this, purchase.isAutoRenewing());
    }
}
