package acina.creative.ashwood.wallet.view_model;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

import acina.creative.ashwood.wallet.database.WalletDatabase;
import acina.creative.ashwood.wallet.database.expense.ExpenseEntry;
import timber.log.Timber;

public class ExpensesViewModel extends AndroidViewModel {

    private LiveData<List<ExpenseEntry>> expenses;
    private WalletDatabase mDb;

    public ExpensesViewModel(@NonNull Application application) {
        super(application);
        mDb = WalletDatabase.getInstance(this.getApplication());
        Timber.d("Actively retrieving the entries from the DataBase" );
        expenses = mDb.expenseDao().loadAllExpenses();
    }

    public LiveData<List<ExpenseEntry>> getExpensesByCategories() {
        return expenses;
    }
}