package acina.creative.ashwood.wallet.interfaces;

import android.view.View;

public interface ItemClickListener {
    void onItemClickListener(View view, int itemId, String name, int url);
}
