package acina.creative.ashwood.wallet.calculator;

public enum MathOperations {
    ADDITION,
    SUBTRACT,
    MULTIPLICATION,
    DIVISION,
}
