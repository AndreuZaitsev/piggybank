package acina.creative.ashwood.wallet.calculator;

import java.math.BigDecimal;

public class MathFunctions {
    public static float round(float number, int decimalPlace) {
        BigDecimal bd = new BigDecimal(number);
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }
}
