/*
package acina.creative.ashwood.wallet.view_model;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import acina.creative.ashwood.wallet.database.WalletDatabase;

public class MonthlyStatViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private final WalletDatabase mDb;
    private String mDate;

    public MonthlyStatViewModelFactory(WalletDatabase mDb, String mDate) {
        this.mDb = mDb;
        this.mDate = mDate;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return  (T) new MonthlyStatViewModel(mDb, mDate);
    }

}
*/
