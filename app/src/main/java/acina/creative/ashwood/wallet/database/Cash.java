package acina.creative.ashwood.wallet.database;

import android.arch.persistence.room.ColumnInfo;

public class Cash {
    @ColumnInfo(name = "cash")
    private float cash;

    public float getCash() {
        return cash;
    }

    public void setCash(float cash) {
        this.cash = cash;
    }
}
