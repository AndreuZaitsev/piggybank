package acina.creative.ashwood.wallet.database;


import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import acina.creative.ashwood.wallet.database.category.CategoryDao;
import acina.creative.ashwood.wallet.database.category.CategoryEntry;
import acina.creative.ashwood.wallet.database.expense.ExpenseDao;
import acina.creative.ashwood.wallet.database.expense.ExpenseEntry;
import acina.creative.ashwood.wallet.database.income.IncomeDao;
import acina.creative.ashwood.wallet.database.income.IncomeEntry;
import acina.creative.ashwood.wallet.utils.AppExecutors;
import timber.log.Timber;

@Database(entities = {CategoryEntry.class, IncomeEntry.class, ExpenseEntry.class}, version = 2, exportSchema = false)
@TypeConverters(DateConverter.class)
public abstract class WalletDatabase extends RoomDatabase {
    private static final java.lang.Object LOCK = new java.lang.Object();
    private static final String DATABASE_NAME = "walletDatabase";
    private static WalletDatabase sInstance;
    // todo: indexes

    public static WalletDatabase getInstance(Context context) {
        if (sInstance == null) {
            synchronized (LOCK) {
                Timber.i("Creating new database instance");
                sInstance =
                        Room.databaseBuilder(context.getApplicationContext(), WalletDatabase.class, WalletDatabase.DATABASE_NAME)
                                .addMigrations(MIGRATION_1_2)
                                .addCallback(new RoomDatabase.Callback() {
                                    @Override
                                    public void onCreate(@NonNull SupportSQLiteDatabase db) {
                                        super.onCreate(db);
                                        AppExecutors.getInstance().diskIO().execute(() -> getInstance(context).categoryDao().initCategories(init()));
                                    }
                                })

                                .build();

            }
        }
        Timber.d("Getting the database instance");
        return sInstance;
    }

    private static List<CategoryEntry> init() {
        List<CategoryEntry> initList = new ArrayList<>();
        initList.add(new CategoryEntry("Food", "ic_food"));
        initList.add(new CategoryEntry("Sport", "ic_sport"));
        initList.add(new CategoryEntry("Payments","ic_mobile"));
        initList.add(new CategoryEntry("Entertainment", "ic_alcohole"));
        initList.add(new CategoryEntry("Clothes", "ic_clothes"));
        return initList;
    }

    static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("CREATE INDEX index_expenses_date ON  expenses(dateF)");
        }
    };
    public abstract CategoryDao categoryDao();
    public abstract IncomeDao incomeDao();
    public abstract ExpenseDao expenseDao();
    public abstract CategoryWithExpensesDao categoryWithExpensesDao();
}
