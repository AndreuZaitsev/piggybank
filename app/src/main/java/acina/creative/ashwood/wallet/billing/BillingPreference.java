package acina.creative.ashwood.wallet.billing;

import android.content.Context;
import android.content.SharedPreferences;

public class BillingPreference {

    private static final String APP_PREFERENCES = "preferences";
    private static final String PREF_SUB_KEY = "isSubscribed";
    private static SharedPreferences mIsSubscribedPref;
    private static Boolean isPay = false  ;

    public static void setmIsSubscribedPref(Context context, Boolean isPay){
        mIsSubscribedPref = context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mIsSubscribedPref.edit();
        editor.putBoolean(PREF_SUB_KEY, isPay);
        editor.apply();

    }
    public static Boolean isSubscribed(Context context) {
        mIsSubscribedPref = context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        return isPay = mIsSubscribedPref.getBoolean(PREF_SUB_KEY, false);
    }

}
