package acina.creative.ashwood.wallet.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import acina.creative.ashwood.wallet.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FaqFragment extends Fragment {

    private static FaqFragment INSTANCE = null;

    public static FaqFragment getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new FaqFragment();
        }
        return INSTANCE;
    }

    public FaqFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_faq, container, false);
    }

}
