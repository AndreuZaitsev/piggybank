package acina.creative.ashwood.wallet.view_model;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

import acina.creative.ashwood.wallet.database.WalletDatabase;
import acina.creative.ashwood.wallet.database.income.IncomeEntry;
import timber.log.Timber;

public class IncomesViewModel extends AndroidViewModel {

    private LiveData<List<IncomeEntry>> incomes;
    private WalletDatabase mDb;

    public IncomesViewModel(@NonNull Application application) {
        super(application);
        mDb = WalletDatabase.getInstance(this.getApplication());
        Timber.d("Actively retrieving the entries from the DataBase" );
        incomes = mDb.incomeDao().loadAllIncomes();
    }

    public LiveData<List<IncomeEntry>> getIncomes() {
        return incomes;
    }
}