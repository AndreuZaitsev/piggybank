package acina.creative.ashwood.wallet.billing;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.transition.Transition;
import android.transition.TransitionInflater;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.Purchase;

import java.util.ArrayList;
import java.util.List;

import acina.creative.ashwood.wallet.R;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

public class BillingActivity extends AppCompatActivity {

    private BillingManager mBillingManager;
    private ArrayList<String> mSkuIdList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        setupWindowAnimations();

        setContentView(R.layout.activity_billing);

        ButterKnife.bind(this);

        mSkuIdList = SKU.getSkuList();
        mBillingManager = new BillingManager(BillingActivity.this, new MyBillingUpdateListener());

    }

    private void setupWindowAnimations() {
        TransitionInflater inflater = TransitionInflater.from(this);
        Transition transition = inflater.inflateTransition(R.transition.explode);
        transition.excludeTarget(android.R.id.statusBarBackground, true);
        transition.excludeTarget(android.R.id.navigationBarBackground, true);
        getWindow().setEnterTransition(transition);
    }

    @OnClick(R.id.btn_subscribe)
    public void subscribe() {
        mBillingManager.initiatePurchaseFlow(mSkuIdList.get(0), null, BillingClient.SkuType.SUBS);
    }


    private void updateUI(Purchase purchase) {
        if (purchase.isAutoRenewing()) {
            // UNLOCK SCREEN (PREMIUM VERSION)
            BillingPreference.setmIsSubscribedPref(this,true);
        } else {
            // LOCK SCREEN (FREE VERSION)
            BillingPreference.setmIsSubscribedPref(this, false);
        }
    }

    class MyBillingUpdateListener implements BillingUpdateListener {

        @Override
        public void onBillingClientSetupFinished() {
            Timber.i("billing: onBillingClientSetupFinished");
            mBillingManager.queryPurchases();

        }

        @Override
        public void onConsumeFinished(String token, int result) {
            if (result == BillingClient.BillingResponse.OK) {
                Timber.i("billing: onConsumeFinished");
            }

        }

        @Override
        public void onPurchasesUpdated(List<Purchase> purchases) {
            Purchase purchase;
            try {
                purchase = purchases.get(0);
                if (purchase != null) {
                    Timber.i("billing: purchase: %s", purchase);
                    updateUI(purchase);
                }
            } catch (IndexOutOfBoundsException e) {
                Timber.e(e, "billing: Error in purchase.get(0)");
            }

        }
    }
}
