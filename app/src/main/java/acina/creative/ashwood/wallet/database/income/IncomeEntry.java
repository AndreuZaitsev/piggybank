package acina.creative.ashwood.wallet.database.income;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "incomes")
public class IncomeEntry {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String date;
    private float cash;

    public IncomeEntry(int id, String date, float cash) {
        this.id = id;
        this.date = date;
        this.cash = cash;
    }

    @Ignore
    public IncomeEntry(String  date, float cash) {
        this.date = date;
        this.cash = cash;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public float getCash() {
        return cash;
    }

    public void setCash(float cash) {
        this.cash = cash;
    }

}
