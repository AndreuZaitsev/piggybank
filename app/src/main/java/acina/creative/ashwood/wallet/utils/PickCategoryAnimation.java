package acina.creative.ashwood.wallet.utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class PickCategoryAnimation {
    private static final float DP = Resources.getSystem().getDisplayMetrics().density;
    private RelativeLayout rootIvIcon;
    private Context mContext;
    private ImageView mIvIconWhite;

    public PickCategoryAnimation(Context context, RelativeLayout rootIvIcon, ImageView ivIconWhite) {
        this.rootIvIcon = rootIvIcon;
        this.mContext = context;
        this.mIvIconWhite = ivIconWhite;
    }

    public void setAnimation(int size, Drawable drawable) {

        ObjectAnimator alphaAnimator = ObjectAnimator.ofFloat(rootIvIcon.getChildAt(1), View.ALPHA, 0.0f, 1.0f);
        alphaAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(final Animator animation) {
                rootIvIcon.getChildAt(1).getLayoutParams().height = (int) (size * DP);
                rootIvIcon.getChildAt(1).getLayoutParams().width = (int) (size * DP);
                if (drawable != null) {
                    // Check
                    rootIvIcon.getChildAt(1).setVisibility(View.VISIBLE);
                } else {
                    // Uncheck
                    rootIvIcon.getChildAt(1).setVisibility(View.GONE);
                }
            }
        });

        ObjectAnimator alphaAnimator2 = ObjectAnimator.ofFloat(rootIvIcon, View.ALPHA, 0.0f, 1.0f);
        alphaAnimator2.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(final Animator animation) {
                if (drawable != null) {
                    // Check
                    rootIvIcon.getChildAt(0).setVisibility(View.INVISIBLE);
                } else {
                    // Uncheck
                    rootIvIcon.getChildAt(0).setVisibility(View.VISIBLE);
                }
                rootIvIcon.setBackground(drawable);
            }
        });

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(alphaAnimator, alphaAnimator2);
        animatorSet.setDuration(300);
        animatorSet.start();
    }
}
