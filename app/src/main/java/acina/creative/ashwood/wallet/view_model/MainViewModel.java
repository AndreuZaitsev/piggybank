package acina.creative.ashwood.wallet.view_model;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

import acina.creative.ashwood.wallet.database.Cash;
import acina.creative.ashwood.wallet.database.WalletDatabase;
import acina.creative.ashwood.wallet.database.category.CategoryEntry;
import timber.log.Timber;

public class MainViewModel extends AndroidViewModel {

    private LiveData<List<CategoryEntry>> categoryEntryList;
    private LiveData<Cash> incomesCash;
    private LiveData<Cash> expensesCash;

    private WalletDatabase mDb;

    public MainViewModel(@NonNull Application application) {
        super(application);
        mDb = WalletDatabase.getInstance(this.getApplication());
        Timber.d("Actively retrieving the entries from the DataBase");
        categoryEntryList = mDb.categoryDao().loadAllCategories();
        incomesCash = mDb.incomeDao().loadSumIncomesEntry();
        expensesCash = mDb.expenseDao().loadSumExpensesEntry();
    }



    public LiveData<List<CategoryEntry>> getCategoryEntryList() {
        return categoryEntryList;
    }

    public LiveData<Cash> getIncomesCash() {
        return incomesCash;
    }

    public LiveData<Cash> getExpensesCash() {
        return expensesCash;
    }

}
