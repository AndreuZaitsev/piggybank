package acina.creative.ashwood.wallet.database.income;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import acina.creative.ashwood.wallet.database.Cash;

@Dao
public interface IncomeDao {
    @Query("SELECT * FROM incomes ORDER BY id DESC")
    LiveData<List<IncomeEntry>> loadAllIncomes();

    @Insert
    void insertIncome(IncomeEntry incomeEntry);

    @Query("SELECT SUM(cash) AS cash FROM incomes")
    LiveData<Cash> loadSumIncomesEntry();

    @Delete
    void deleteIncome(IncomeEntry incomeEntry);
}


