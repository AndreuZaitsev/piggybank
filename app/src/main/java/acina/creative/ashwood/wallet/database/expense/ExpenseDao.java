package acina.creative.ashwood.wallet.database.expense;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import acina.creative.ashwood.wallet.database.Cash;

@Dao
public interface ExpenseDao {
    @Insert
    long insertExpenseEntry(ExpenseEntry expenseEntry);

    @Query("SELECT * FROM expenses ORDER BY id DESC")
    LiveData<List<ExpenseEntry>> loadAllExpenses();

    @Query("SELECT id, dateF, SUM(cash) AS cash, categoryId FROM expenses GROUP BY categoryId")
    LiveData<List<ExpenseEntry>> loadSumExpensesByCategories();

    @Query("SELECT id, dateF, SUM(cash) AS cash, categoryId FROM expenses WHERE substr(dateF, 4, 10) = :monthVsYear GROUP BY categoryId")
    LiveData<List<ExpenseEntry>> loadSumExpensesByCategoriesByMonth(String monthVsYear);

    @Query("SELECT id, dateF, SUM(cash) AS cash, categoryId FROM expenses WHERE substr(dateF, 4, 10) = :monthVsYear GROUP BY categoryId")
    List<ExpenseEntry> loadSumExpensesByCategoriesByMonthNotLive(String monthVsYear);

    @Query("SELECT id, dateF, SUM(cash) AS cash, categoryId FROM expenses WHERE substr(dateF, 7, 10) = :year GROUP BY categoryId")
    List<ExpenseEntry> loadSumExpensesByCategoriesByYearNotLive(String year);

    @Query("SELECT id, dateF, SUM(cash) AS cash, categoryId FROM expenses WHERE substr(dateF, 7, 10) = :year GROUP BY categoryId")
    LiveData<List<ExpenseEntry>> loadSumExpensesByCategoriesByYear(String year);

    @Query("SELECT id, dateF, SUM(cash) AS cash, categoryId FROM expenses GROUP BY dateF, categoryId ORDER BY id DESC")
    LiveData<List<ExpenseEntry>> loadSumExpensesByCategoriesDaily();

    @Query("SELECT id, dateF, SUM(cash) AS cash, categoryId FROM expenses GROUP BY substr(dateF, 4, 10), categoryId")
    LiveData<List<ExpenseEntry>> loadSumExpensesByCategoriesMonthly();

    @Query("SELECT id, dateF, SUM(cash) AS cash, categoryId FROM expenses GROUP BY substr(dateF, 7, 10), categoryId")
    LiveData<List<ExpenseEntry>> loadSumExpensesByCategoriesYearly();

    @Query("SELECT SUM(cash) AS cash FROM expenses")
    LiveData<Cash> loadSumExpensesEntry();

    @Query("DELETE FROM expenses")
    void deleteAllRecords();

    @Delete
    void deleteExpense(ExpenseEntry expenseEntry);

    @Query("DELETE FROM expenses WHERE id = :id")
    void deleteExpenseById(int id);


}
