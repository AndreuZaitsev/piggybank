package acina.creative.ashwood.wallet.view_model;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import acina.creative.ashwood.wallet.database.Cash;
import acina.creative.ashwood.wallet.database.WalletDatabase;
import timber.log.Timber;

public class AddIncomeViewModel extends AndroidViewModel {
    private LiveData<Cash> incomesCash;
    private LiveData<Cash> expensesCash;

    private WalletDatabase mDb;

    public AddIncomeViewModel(@NonNull Application application) {
        super(application);
        mDb = WalletDatabase.getInstance(this.getApplication());
        Timber.d("Actively retrieving the entries from the DataBase");
        incomesCash = mDb.incomeDao().loadSumIncomesEntry();
        expensesCash = mDb.expenseDao().loadSumExpensesEntry();
    }


    public LiveData<Cash> getIncomesCash() {
        return incomesCash;
    }

    public LiveData<Cash> getExpensesCash() {
        return expensesCash;
    }
}
