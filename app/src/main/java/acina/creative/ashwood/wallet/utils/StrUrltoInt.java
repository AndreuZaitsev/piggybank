package acina.creative.ashwood.wallet.utils;

import android.content.Context;
import android.content.res.Resources;

public class StrUrltoInt {


    public static int strToInt(Context context, String name){
        Resources resources = context.getResources();
        return resources.getIdentifier(name, "drawable",
                context.getPackageName());
    }

    public static String intToStr(Context context, int id){
        Resources resources = context.getResources();
        return resources.getResourceEntryName(id);
    }


}
