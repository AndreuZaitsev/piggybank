package acina.creative.ashwood.wallet.adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import acina.creative.ashwood.wallet.R;
import acina.creative.ashwood.wallet.statistic_fragments.DailyStatFragment;
import acina.creative.ashwood.wallet.statistic_fragments.MonthlyStatFragment;
import acina.creative.ashwood.wallet.statistic_fragments.YearlyStatFragment;

public class StatFragmentsAdapter extends FragmentPagerAdapter {
    private static final int STAT_FRAGMENTS_COUNT = 3;
    private Context mContext;

    public StatFragmentsAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.mContext = context;
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0:
                return DailyStatFragment.getInstance();
            case 1:
                return MonthlyStatFragment.getInstance();
            case 2:
                return YearlyStatFragment.getInstance();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return STAT_FRAGMENTS_COUNT;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return mContext.getString(R.string.tab1);
            case 1:
                return mContext.getString(R.string.tab2);
            case 2:
                return mContext.getString(R.string.tab3);
            default:
                return "";
        }
    }


}
