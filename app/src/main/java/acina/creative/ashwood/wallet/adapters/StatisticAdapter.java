package acina.creative.ashwood.wallet.adapters;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import acina.creative.ashwood.wallet.R;
import acina.creative.ashwood.wallet.database.WalletDatabase;
import acina.creative.ashwood.wallet.database.category.CategoryEntry;
import acina.creative.ashwood.wallet.database.expense.ExpenseEntry;
import acina.creative.ashwood.wallet.interfaces.ItemClickListener;
import acina.creative.ashwood.wallet.utils.AppExecutors;
import butterknife.BindView;
import butterknife.ButterKnife;

public class StatisticAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    final private ItemClickListener mItemClickListener;

    private Context mContext;
    private List<Object> mObjectsList;
    private List<ExpenseEntry> mExpenseEntryList;

    private final int HEADER = 0, LIST = 1;

    /*
     * Constructor for the Adapter that initialize the Context
     * */
    public StatisticAdapter(Context mContext, ItemClickListener mItemClickListener) {
        this.mContext = mContext;
        this.mItemClickListener = mItemClickListener;
    }

    /*
     * When data changes, this method updates the list of categoryEntries
     * and notifies the adapter to use the new values on it
     * */
    public void setExpenseEntries(List<Object> expenseEntryList) {
        mObjectsList = expenseEntryList;
        notifyDataSetChanged();
    }

    /*
     * Get the list of categoryEntries
     * */
    public List<Object> getExpensesEntries() {
        return mObjectsList;
    }

    @Override
    public int getItemViewType(int position) {
        if (mObjectsList.get(position) instanceof ExpenseEntry) {
            return LIST;
        } else {
            return HEADER;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        if (viewType == HEADER) {
            View v1 = inflater.inflate(R.layout.item_date, viewGroup, false);
            viewHolder = new ExpenseDateViewHolder(v1);
        } else {
            View v2 = inflater.inflate(R.layout.item_expense, viewGroup, false);
            viewHolder = new ExpenseViewHolder(v2);
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        switch (viewHolder.getItemViewType()) {
            case HEADER:
                ExpenseDateViewHolder vh1 = (ExpenseDateViewHolder) viewHolder;
                configureViewHolder1(vh1, position);
                break;
            case LIST:
                ExpenseViewHolder vh2 = (ExpenseViewHolder) viewHolder;
                configureViewHolder2(vh2, position);
                break;
        }
    }

    private void configureViewHolder2(ExpenseViewHolder holder, int position) {
        // Set values
        AppExecutors.getInstance().diskIO().execute(() -> {
            ExpenseEntry entry = (ExpenseEntry) mObjectsList.get(position);
            CategoryEntry categoryEntry = WalletDatabase.getInstance(mContext).categoryDao().loadCategoryById(entry.getCategoryId());
            new Handler(Looper.getMainLooper()).post(() -> {
                holder.tvCategoryName.setText(categoryEntry.getName());
                String cash = String.valueOf(entry.getCash());
                holder.tvCash.setText(cash);
                holder.tvDate.setVisibility(View.GONE);
            });

        });
    }

    private void configureViewHolder1(ExpenseDateViewHolder holder, int position) {
        String date = (String) mObjectsList.get(position);
        holder.tvDate.setText(date);
    }

    @Override
    public int getItemCount() {
        if (mObjectsList == null) {
            return 0;
        }
        return mObjectsList.size();

    }

    // Inner class for creating ViewHolders
     class ExpenseViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener {

        @BindView(R.id.tv_category_name)
        TextView tvCategoryName;

        @BindView(R.id.tv_cash)
        TextView tvCash;

        @BindView(R.id.tv_date)
        TextView tvDate;

        private ExpenseViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void onClick(View view) {
            if (mObjectsList.get(getAdapterPosition()) instanceof ExpenseEntry) {
                ExpenseEntry entry = (ExpenseEntry) mObjectsList.get(getAdapterPosition());
                int i = entry.getId();
                mItemClickListener.onItemClickListener(view, i, "", 1);
            }
        }
    }

    class ExpenseDateViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_header_date)
        TextView tvDate;

        private ExpenseDateViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

}
