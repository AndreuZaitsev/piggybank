package acina.creative.ashwood.wallet.model;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import acina.creative.ashwood.wallet.R;
import acina.creative.ashwood.wallet.adapters.UserCategoriesAdapter;
import acina.creative.ashwood.wallet.database.WalletDatabase;
import acina.creative.ashwood.wallet.database.category.CategoryEntry;
import acina.creative.ashwood.wallet.interfaces.ItemLongClickListener;
import acina.creative.ashwood.wallet.utils.AppExecutors;
import acina.creative.ashwood.wallet.utils.PickCategoryAnimation;
import acina.creative.ashwood.wallet.utils.RecyclerTouchListener;
import acina.creative.ashwood.wallet.utils.StrUrltoInt;
import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class AddCategoryActivity extends AppCompatActivity {

    @BindView(R.id.add_category_toolbar)
    Toolbar mToolbar;

    @BindView(R.id.add_category_root)
    RelativeLayout mRootLayout;

    @BindView(R.id.add_category_recyclerview)
    RecyclerView mAllCategoryRecyclerView;

    @BindView(R.id.edit_category_name)
    EditText mEditCategoryName;


    private UserCategoriesAdapter mAdapter;

    private WalletDatabase mDb;

    @BindDrawable(R.drawable.bg_checked)
    Drawable mCheckedIcon;

    private final String KEY_CATEGORY = "checked_category_key";
    private final int NO_CATEGORY = -1;
    private Bundle mCheckedCategory = new Bundle();
    private ImageView ivIconWhite;
    private PickCategoryAnimation mAnim;
    private int mCategotyIconUrl = -1;
    public int currentScrollPosition = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_category);
        ButterKnife.bind(this);

        mDb = WalletDatabase.getInstance(this);

        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        setupRecyclerView();

        mCheckedCategory.putInt(KEY_CATEGORY, NO_CATEGORY);
    }

    private void setupRecyclerView() {
        mAllCategoryRecyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        mAllCategoryRecyclerView.setHasFixedSize(true);

        mAdapter = new UserCategoriesAdapter(this);
        mAllCategoryRecyclerView.setAdapter(mAdapter);

        mAllCategoryRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentScrollPosition = recyclerView.computeVerticalScrollOffset() + recyclerView.computeVerticalScrollExtent();
                LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                if (layoutManager != null && mIconList().size() - 1 == layoutManager.findLastCompletelyVisibleItemPosition()) {
                    InputMethodManager in = (InputMethodManager) AddCategoryActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    View view = AddCategoryActivity.this.getCurrentFocus();
                    if (view == null) {
                        view = new View(AddCategoryActivity.this);
                    }
                    in.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    view.clearFocus();
                }
            }
        });

        mAllCategoryRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, mAllCategoryRecyclerView, new ItemLongClickListener() {
            @Override
            public void onClick(View view, int position) {
                String url = mIconList().get(position).getUrl();
                int itemUrl = StrUrltoInt.strToInt(getApplicationContext(), url);
                pickCategory(view, position, itemUrl);
            }

            @Override
            public void onItemLongClickListener(View view, int itemId) {
            }
        }));

    }

    private void pickCategory(View view, int itemId, int itemUrl) {
        int check = mCheckedCategory.getInt(KEY_CATEGORY);

        if (check != itemId && ivIconWhite != null) {
            // Uncheck previous
            clearAllSelection();
        }

        ivIconWhite = view.findViewById(R.id.iv_category_icon_white);

        RelativeLayout rootIvIcon = view.findViewById(R.id.root_iv_category_icon);
        mAnim = new PickCategoryAnimation(getApplicationContext(), rootIvIcon, ivIconWhite);
        check = mCheckedCategory.getInt(KEY_CATEGORY);

        if (check == NO_CATEGORY) {
            // Check
            mCheckedCategory.putInt(KEY_CATEGORY, itemId);
            mAnim.setAnimation(30, mCheckedIcon);
            mCategotyIconUrl = itemUrl;

        } else {
            // Uncheck
            clearAllSelection();
        }
    }

    // Init categories list
    private List<CategoryEntry> mIconList() {
        List<CategoryEntry> list = new ArrayList<>();
        list.add(new CategoryEntry(1, "Food", "ic_food"));
        list.add(new CategoryEntry(2, "Car", "ic_car"));
        list.add(new CategoryEntry(3, "Sport", "ic_sport"));
        list.add(new CategoryEntry(4, "Mobile", "ic_mobile"));
        list.add(new CategoryEntry(5, "Party", "ic_alcohole"));
        list.add(new CategoryEntry(6, "Clothes", "ic_clothes"));
        list.add(new CategoryEntry(7, "Relax", "ic_sea"));
        list.add(new CategoryEntry(8, "Internet", "ic_internet"));
        list.add(new CategoryEntry(9, "Pet", "ic_pet"));
        list.add(new CategoryEntry(10, "Repair", "ic_repair"));
        list.add(new CategoryEntry(11, "Cafe", "ic_cooking"));
        list.add(new CategoryEntry(12, "Furniture", "ic_sofa"));
        list.add(new CategoryEntry(13, "Gift", "ic_gift"));
        list.add(new CategoryEntry(14, "Media", "ic_media"));
        return list;
    }

    @OnClick(R.id.fab_insert)
    public void insertFabClick(View view) {
        String name = mEditCategoryName.getText().toString();
        if (!name.isEmpty()) {
            if (mCategotyIconUrl != -1) {
                ObjectAnimator.ofFloat(view, View.ROTATION, 0f, 360f).setDuration(450).start();
                final Handler handler = new Handler();
                handler.postDelayed(() -> insertNewCategory(name), 450);
            } else {
                Toast.makeText(this, "choose category icon", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "set category name", Toast.LENGTH_SHORT).show();
        }

    }

    private void insertNewCategory(String name) {
        String mCategoryName = name.substring(0, 1).toUpperCase() + name.substring(1);
        CategoryEntry categoryEntry = new CategoryEntry(mCategoryName, StrUrltoInt.intToStr(getApplicationContext(), mCategotyIconUrl));
        AppExecutors.getInstance().diskIO().execute(() -> {
            mDb.categoryDao().insertCategory(categoryEntry);
            runOnUiThread(this::clearAllSelection);
            finish();
        });


    }

    private void clearAllSelection() {
        mCheckedCategory.putInt(KEY_CATEGORY, NO_CATEGORY);
        mCategotyIconUrl = -1;
        if (mAnim != null) {
            mAnim.setAnimation(60, null);
        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    // LifeCycle Events
    @Override
    protected void onResume() {
        super.onResume();
        mAdapter.setCategoryEntries(mIconList());
    }

    @Override
    protected void onStop() {
        super.onStop();
        clearAllSelection();
    }

}
