package acina.creative.ashwood.wallet.calculator;

public final class MathSigns {
    public static final String PLUS = "+";
    public static final String MINUS = "-";
    public static final String MULTIPLY = "*";
    public static final String DIVIDE = "/";
    public static final String EQUAL = "=";
    public static final String DOT = ".";

}
