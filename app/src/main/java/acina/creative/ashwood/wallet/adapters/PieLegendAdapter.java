package acina.creative.ashwood.wallet.adapters;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.mikephil.charting.data.PieData;

import java.util.Locale;

import acina.creative.ashwood.wallet.R;
import acina.creative.ashwood.wallet.calculator.MathFunctions;
import acina.creative.ashwood.wallet.database.WalletDatabase;
import acina.creative.ashwood.wallet.database.category.CategoryEntry;
import acina.creative.ashwood.wallet.database.expense.ExpenseEntry;
import acina.creative.ashwood.wallet.interfaces.ItemClickListener;
import acina.creative.ashwood.wallet.utils.AppExecutors;
import butterknife.BindView;
import butterknife.ButterKnife;

public class PieLegendAdapter extends RecyclerView.Adapter<PieLegendAdapter.PieLegendViewHolder> {

    final private ItemClickListener mItemClickListener;

    private Context mContext;
    private PieData pieData;

    public PieLegendAdapter(Context mContext, ItemClickListener itemClickListener) {
        this.mContext = mContext;
        this.mItemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public PieLegendAdapter.PieLegendViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_stat_expense, viewGroup, false);
        return new PieLegendViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PieLegendViewHolder holder, int position) {

        ExpenseEntry entry = (ExpenseEntry) pieData.getDataSet().getEntryForIndex(position).getData();

        AppExecutors.getInstance().diskIO().execute(() -> {
            CategoryEntry categoryEntry = WalletDatabase.getInstance(mContext).categoryDao().loadCategoryById(entry.getCategoryId());
            new Handler(Looper.getMainLooper()).post(() -> {
                holder.tvCategoryName.setText(categoryEntry.getName());

                float cash = entry.getCash();
                holder.tvCash.setText(String.format(Locale.getDefault(),"%,.2f", cash));

                float percentf = MathFunctions.round((entry.getCash() * 100.0f) / pieData.getYValueSum(), 2);
                holder.tvPercent.setText(String.format(Locale.getDefault(), "%,.2f", percentf) + "%");

                holder.tvDate.setVisibility(View.GONE);

                int color = pieData.getDataSet().getColor(position);
                holder.ivIcon.getDrawable().setTint(color);
            });
        });

    }

    @Override
    public int getItemCount() {
        if (pieData == null) {
            return 0;
        }
        return pieData.getEntryCount();
    }


    public PieData getPieData() {
        return pieData;
    }

    public void setPieData(PieData list) {
        pieData = list;
        notifyDataSetChanged();
    }


    public class PieLegendViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.iv_category_icon)
        ImageView ivIcon;

        @BindView(R.id.tv_category_name)
        TextView tvCategoryName;

        @BindView(R.id.tv_cash)
        TextView tvCash;

        @BindView(R.id.tv_cash_percent)
        TextView tvPercent;

        @BindView(R.id.tv_date)
        TextView tvDate;

        public PieLegendViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mItemClickListener.onItemClickListener(v, 1, "", 1);
        }
    }
}
