package acina.creative.ashwood.wallet.database.category;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;

import java.util.List;

@Dao
public interface CategoryDao {

//    @Transaction
//    @Query("SELECT id, name, url, cash from categories")
//    List<CategoryWithExpenses> getCategoriesWithIncomes();

    @Query("SELECT * FROM categories")
    LiveData<List<CategoryEntry>> loadAllCategories();

    @Insert
    void insertCategory(CategoryEntry categoryEntry);

    @Transaction
    @Insert
    void initCategories(List<CategoryEntry> categoryEntries);

    @Delete
    void deleteCategory(CategoryEntry categoryEntry);

    @Query("DELETE FROM categories")
    void deleteAllCategories();

    @Query("SELECT * FROM categories WHERE id = :id")
    CategoryEntry loadCategoryById(int id);

}
