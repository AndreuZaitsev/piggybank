package acina.creative.ashwood.wallet.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;

import java.util.List;

@Dao
public interface CategoryWithExpensesDao {

    @Transaction
    @Query("SELECT * from categories")
    List<CategoryWithExpenses> getCategoriesWithExpenses();
}
