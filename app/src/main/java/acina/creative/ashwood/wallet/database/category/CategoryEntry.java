package acina.creative.ashwood.wallet.database.category;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "categories")
public class CategoryEntry {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private String name;
    private String url;


    public CategoryEntry(int id, String name, String url) {
        this.id = id;
        this.name = name;
        this.url = url;
    }

    @Ignore
    public CategoryEntry(String name, String url) {
        this.name = name;
        this.url = url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


}
