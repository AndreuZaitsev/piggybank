package acina.creative.ashwood.wallet.model;

import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.Group;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;

import acina.creative.ashwood.wallet.R;
import acina.creative.ashwood.wallet.calculator.MathFunctions;
import acina.creative.ashwood.wallet.calculator.MathOperations;
import acina.creative.ashwood.wallet.calculator.MathSigns;
import acina.creative.ashwood.wallet.database.WalletDatabase;
import acina.creative.ashwood.wallet.database.income.IncomeEntry;
import acina.creative.ashwood.wallet.utils.AppExecutors;
import acina.creative.ashwood.wallet.utils.DateFormat;
import acina.creative.ashwood.wallet.view_model.AddIncomeViewModel;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static acina.creative.ashwood.wallet.calculator.MathOperations.ADDITION;
import static acina.creative.ashwood.wallet.calculator.MathOperations.SUBTRACT;

public class ChangeBudgetActivity extends AppCompatActivity {

//    private static final String TAG = "MainFragment";

    @BindView(R.id.change_budget_toolbar)
    Toolbar mToolbar;

    @BindView(R.id.editCash)
    EditText mEditCash;

    @BindView(R.id.tv_budget)
    TextView tvBudget;

    @BindView(R.id.calculate_group)
    Group mCalculateGroup;

    /* Calculator buttons */
    @BindView(R.id.calc_0)
    Button calc_0_btn;
    @BindView(R.id.calc_1)
    Button calc_1_btn;
    @BindView(R.id.calc_2)
    Button calc_2_btn;
    @BindView(R.id.calc_3)
    Button calc_3_btn;
    @BindView(R.id.calc_4)
    Button calc_4_btn;
    @BindView(R.id.calc_5)
    Button calc_5_btn;
    @BindView(R.id.calc_6)
    Button calc_6_btn;
    @BindView(R.id.calc_7)
    Button calc_7_btn;
    @BindView(R.id.calc_8)
    Button calc_8_btn;
    @BindView(R.id.calc_9)
    Button calc_9_btn;
    @BindView(R.id.calc_dot)
    Button calc_dot_btn;
    @BindView(R.id.calc_minus)
    Button calc_minus_btn;
    @BindView(R.id.calc_plus)
    Button calc_plus_btn;
    @BindView(R.id.calc_equal)
    Button calc_equal_btn;
    @BindView(R.id.calc_delete)
    Button calc_delete_btn;
    @BindView(R.id.calc_clear)
    Button calc_clear_btn;

    private static final String ARITHMETIC_OPERATION_KEY = "ARITHMETIC_OPERATION_KEY";
    private float mValueOne, mValueTwo;
    private Bundle mMathOperationBundle = new Bundle();

    private WalletDatabase mDb;
    private float mBalance, mTotalIncomes, mTotalExpenses;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_budget);

        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        mDb = WalletDatabase.getInstance(this);

        setupViewModel();

    }

    private void setupViewModel() {
        AddIncomeViewModel mainViewModel = ViewModelProviders.of(this).get(AddIncomeViewModel.class);

        // Update balance with income
        mainViewModel.getIncomesCash().observe(this, cash -> {
            mTotalIncomes = cash != null ? cash.getCash() : 0;
            mBalance = mTotalIncomes - mTotalExpenses;
            tvBudget.setText(String.valueOf(mBalance));
        });
        // Update balance with expense
        mainViewModel.getExpensesCash().observe(this, cash -> {
            mTotalExpenses = cash != null ? cash.getCash() : 0;
            mBalance = mTotalIncomes - mTotalExpenses;
            tvBudget.setText(String.valueOf(mBalance));
        });
    }

    @OnClick(R.id.save_income)
    public void saveIncome() {
        float cash;
        try {
            cash = Float.parseFloat(mEditCash.getText().toString());
            if (cash == 0) throw new Exception();
            String date = DateFormat.dateToString(new Date());
            IncomeEntry incomeEntry = new IncomeEntry(date, cash);
            AppExecutors.getInstance().diskIO().execute(() -> mDb.incomeDao().insertIncome(incomeEntry));
            mEditCash.setText("");
            mValueOne = 0;
            mValueTwo = 0;
        } catch (NumberFormatException e) {
            Toast.makeText(this, "Wrong value", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(this, "Use a value greater than zero", Toast.LENGTH_SHORT).show();
        }

    }

    @OnClick(R.id.edit)
    public void intentToEdit() {
        Intent intent = new Intent();
        intent.putExtra("EDIT_INTENT", "incomes");
        setResult(RESULT_OK, intent);
        finish();
    }

    // Sets the calculator buttons click listener
    @SuppressLint("SetTextI18n")
    @OnClick({R.id.calc_0, R.id.calc_1, R.id.calc_2, R.id.calc_3,
            R.id.calc_4, R.id.calc_5, R.id.calc_6,
            R.id.calc_7, R.id.calc_8, R.id.calc_9,
            R.id.calc_dot, R.id.calc_minus, R.id.calc_plus,
            R.id.calc_equal, R.id.calc_delete, R.id.calc_clear})
    public void calcBtnOnClick(Button btn) {
        String sign = btn.getText().toString();

        if (btn.getId() == R.id.calc_delete) {
            calcDeleteLast();

        } else if (btn.getId() == R.id.calc_clear) {
            calcClear();

        } else if (sign.equals(MathSigns.EQUAL)) {
            calcEqual();

        } else if (sign.equals(MathSigns.PLUS)) {
            calcMathSign(sign);

        } else if (sign.equals(MathSigns.MINUS)) {
            calcMathSign(sign);

        } else if (sign.equals(MathSigns.DOT)) {
            calcMathSign(sign);

        } else {
            // That's mean user has clicked a number
            mEditCash.setText(mEditCash.getText() + sign);
        }
    }

    private void calcEqual() {
        String text = String.valueOf(mEditCash.getText());
        if (text.length() > 0) {
            String lastChar = String.valueOf(mEditCash.getText().charAt(text.length() - 1));
            if (!lastChar.equals(MathSigns.PLUS) && !lastChar.equals(MathSigns.PLUS) && !lastChar.equals(MathSigns.DOT)) {
                String[] result = text.split("(?<=[-+*/])|(?=[-+*/])");
                for (int i = 0; i < result.length; i++) {

                    String val = result[i];
                    switch (val) {
                        case MathSigns.PLUS:
                            mMathOperationBundle.putSerializable(ARITHMETIC_OPERATION_KEY, ADDITION);
                            break;
                        case MathSigns.MINUS:
                            mMathOperationBundle.putSerializable(ARITHMETIC_OPERATION_KEY, SUBTRACT);
                            break;
                        default:
                            if (i == 0) {
                                mValueOne = Float.parseFloat(val);
                            } else {
                                mValueTwo = Float.parseFloat(val);
                                doMath();
                            }
                            break;
                    }
                }
                String sum = String.valueOf(MathFunctions.round(mValueOne, 2));
                mEditCash.setText(sum);
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private void calcMathSign(String sign) {
        int length = mEditCash.getText().length();
        String value = mEditCash.getText().toString();
        if (length > 0) {
            String lastChar = String.valueOf(mEditCash.getText().charAt(length - 1));
            if (!lastChar.equals(MathSigns.PLUS) && !lastChar.equals(MathSigns.MINUS) && !lastChar.equals(MathSigns.DOT)) {
                mEditCash.setText(value + sign);
            }
//            else {
////                Toast.makeText(this, "Wrong argument", Toast.LENGTH_SHORT).show();
//            }
        }
//        else {
////            Toast.makeText(this, "Value first", Toast.LENGTH_SHORT).show();
//        }
    }

    private void calcDeleteLast() {
        int length = mEditCash.getText().length();
        if (length > 0) {
            mEditCash.getText().delete(length - 1, length);
        }
    }

    private void calcClear() {
        mEditCash.setText("");
        mValueOne = 0;
        mValueTwo = 0;
    }

    private void doMath() {
        MathOperations mathOperationType = (MathOperations) mMathOperationBundle.get(ARITHMETIC_OPERATION_KEY);
        if (mathOperationType != null) {
            switch (mathOperationType) {
                case ADDITION:
                    mValueOne += mValueTwo;
                    break;
                case SUBTRACT:
                    mValueOne -= mValueTwo;
            }
        }
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
